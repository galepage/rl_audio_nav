#!/bin/sh

set -e

INSTALL_ROOT=${ASR_INSTALL_ROOT:-"/home/galepage/scratch/inria/code/asr"}
KALDI_ROOT=$INSTALL_ROOT/kaldi

# Vosk with GPU support
N_PROC=$(nproc)


install_openblas_clapack() {
    cd $KALDI_ROOT/tools
    [ -d OpenBLAS ] || git clone -b v0.3.13 --single-branch https://github.com/xianyi/OpenBLAS
    [ -d clapack ] || git clone -b v3.2.1  --single-branch https://github.com/alphacep/clapack
    make -C OpenBLAS ONLY_CBLAS=1 DYNAMIC_ARCH=1 TARGET=NEHALEM USE_LOCKING=1 USE_THREAD=0 all
    make -C OpenBLAS PREFIX=$(pwd)/OpenBLAS/install install
    mkdir -p clapack/BUILD
    cd clapack/BUILD
    cmake ..
    make -j $N_PROC
    find . -name "*.a" | xargs cp -t ../../OpenBLAS/install/lib
}

install_openfst() {

    echo ">> INSTALLING OPENFST"
    cd $KALDI_ROOT/tools

    [ -d openfst ] || git clone --single-branch https://github.com/alphacep/openfst openfst
    cd openfst
    autoreconf -i
    CFLAGS="-g -O3" ./configure --prefix=$KALDI_ROOT/tools/openfst --enable-static --enable-shared --enable-far --enable-ngram-fsts --enable-lookahead-fsts --with-pic --disable-bin
    make -j $N_PROC
    make install
}

install_kaldi() {
    cd $INSTALL_ROOT
    [ -d kaldi ] || git clone -b vosk --single-branch https://github.com/alphacep/kaldi

    install_openblas_clapack
    install_openfst

    echo ">> COMPILING KALDI"
    # Compile `cub`
    cd $KALDI_ROOT/tools
    make cub
    cd $KALDI_ROOT/src
    ./configure --mathlib=OPENBLAS_CLAPACK --shared --use-cuda=yes
    sed -i 's:-msse -msse2:-msse -msse2:g' kaldi.mk
    sed -i 's: -O1 : -O3 :g' kaldi.mk
    make -j $N_PROC online2 lm rnnlm
    find $KALDI_ROOT -name "*.o" -exec rm {} \;
}


install_vosk() {
    cd $INSTALL_ROOT
    [ -d vosk-api ] || git clone https://github.com/alphacep/vosk-api
    cd vosk-api/src

    KALDI_ROOT=$KALDI_ROOT HAVE_CUDA=1 make -j $N_PROC
    cd ../python
    python setup.py install
}

install_kaldi
install_vosk
