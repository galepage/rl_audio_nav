#!/bin/bash

echo "Hostname: $(hostname)"
echo "Python: $(which python)"
echo "Python version: $(python --version)"
echo "Current directory: $(pwd)"
# Cuda version
command -v nvcc > /dev/null 2>&1 && nvcc --version || /usr/local/cuda/bin/nvcc --version
echo ""
nvidia-smi

# python -m torch.utils.collect_env

# SCRIPT="audio_simulator/wer_map/create_wer_map"
# SCRIPT="audio_simulator/test"
# SCRIPT="rl/agent/test_loop"
# SCRIPT="asr/speech_data_set"
# SCRIPT="asr/async_wer_dataset"
SCRIPT="rl/agent/ppo/run"
# SCRIPT="asr/speechbrain/compute_wer_on_data_set"
# SCRIPT="asr/vosk/compute_wer_on_data_set"

COMMAND="python rl_audio_nav/$SCRIPT.py"

# export CUDA_LAUNCH_BLOCKING=1
echo CUDA_LAUNCH_BLOCKING=$CUDA_LAUNCH_BLOCKING

# export HUGGINGFACE_HUB_CACHE=/mnt/beegfs/perception/galepage/tmp
export HUGGINGFACE_HUB_CACHE=/tmp

# Run the script
/usr/bin/time -f "Command took %E" $COMMAND
