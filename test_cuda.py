import torch


def main() -> None:
    from torch.utils import collect_env
    collect_env.main()
    print("torch version:", torch.__version__)
    print("cuda built ?", torch.backends.cuda.is_built())
    print("cuda version:", torch.version.cuda)
    print("cuda available ?", torch.cuda.is_available())
    print("torch.cudnn.benchmark :", torch.backends.cudnn.benchmark)
    print("cudnn version ?", torch.backends.cudnn.version())

    # a = torch.Tensor([1, 2, 3])
    # a.to('cuda:0')

    a = torch.Tensor([[[1, 2, 3, 4],
                       [5, 6, 7, 8]]])
    print(a)
    print(a.shape)
    a = torch.reshape(a, (2, 4))
    a = torch.squeeze(a)
    print(a)
    print(a.shape)
    # obs = obs.to('cuda:0')
    # obs = obs.cuda()


if __name__ == '__main__':
    main()
