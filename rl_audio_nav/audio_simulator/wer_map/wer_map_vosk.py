import logging
from threading import Thread
from time import time

import exputils as eu
import numpy as np

from ..audio_signal import AudioSignal, SpeechSignal
from ..room import (Room,
                    GpuRirRoom,
                    Source)
from ... import asr
from ...asr import AsrDecoder
from ...asr.vosk import VoskAsr

LOGGER: logging.Logger = logging.getLogger()

DEFAULT_CONFIG: eu.AttrDict = eu.AttrDict()


def _thread_worker(dataset: list[SpeechSignal],
                   result_dict: dict,
                   job_id: int,
                   config: eu.AttrDict) -> None:
    """
    Loop through the dataset and simulate the listened signal at each grid position.

    This function is called by each thread and his it own room.

    Args:
        dataset (list[SpeechSignal]):   A subpart of the dataset.
        result_dict (dict):             A sha
    """
    # Create room
    room: Room = GpuRirRoom(config=config.room)

    # Initialize the microphone array
    room.init_grid(add_grid_mics=True)

    n_mics: int = len(room.microphones)
    LOGGER.info("grid size: %ix%i = %i microphones", room.n_x, room.n_y, n_mics)

    # Initialize ASR recognizer
    asr_decoder: AsrDecoder = VoskAsr(use_gpu=True,
                                      in_thread=True)

    num_samples: int = len(dataset)
    LOGGER.info("num_samples: %i", num_samples)

    speech_source: Source = Source(name='speech',
                                   location=np.array([config.speech_x,
                                                      config.speech_y,
                                                      1.8]))
    room.add_source(speech_source)

    if config.noise:
        noise_signal: AudioSignal = AudioSignal(name='noise',
                                                file_path='data/guitar_16k.wav')

        noise_source: Source = Source(name='noise',
                                      location=np.array([config.noise_x,
                                                         config.noise_y,
                                                         1.8]))
        room.add_source(noise_source)
        noise_signal.load_audio()
        room.set_source_input_audio_signal(input_audio_signal=noise_signal.signal,
                                           source_name='noise')

        noise_source2: Source = Source(name='noise2',
                                       location=np.array([2, 1, 1.8]))
        room.add_source(noise_source2)
        room.set_source_input_audio_signal(input_audio_signal=noise_signal.signal,
                                           source_name='noise2')

    # Pre compute the RIR
    start_time: float = time()
    room.pre_compute_rir()
    logging.info("Done computing RIR (took %fs)",
                 time() - start_time)

    wer_matrix: np.ndarray = np.zeros(shape=(room.n_x, room.n_y))
    LOGGER.debug('wer_matrix %s', wer_matrix.shape)

    # For each data sample from the data set add the corresponding speech source.
    # with alive_bar(num_samples) as progress_bar:

    for speech_signal in dataset:
        speech_signal.load_audio()

#         asr_result: str = asr_decoder.decode(signal=speech_signal.signal)
#
#         wer: float = asr.compute_wer(ground_truth=speech_signal.transcript,
#                                      hypothesis=asr_result)

        room.set_source_input_audio_signal(input_audio_signal=speech_signal.signal,
                                           source_name='speech')

        wer_list: list[float] = []

        # For each grid point get the signal and get the WER at this location.
        for mic_index in range(n_mics):
            # print(f"{mic_index}/{n_mics}")
            listened_signal: np.ndarray = room.get_audio_at_mic(mic_index=mic_index,
                                                                save=True,
                                                                save_dir=config.samples_save_dir)

            asr_result = asr_decoder.decode(signal=listened_signal)

            wer = asr.compute_wer(ground_truth=speech_signal.transcript,
                                  hypothesis=asr_result)

            # Save the mean WER in the matrix.
            wer_list.append(wer)

        assert len(wer_list) == room.n_x * room.n_y
        wer_matrix += np.array(wer_list).reshape((room.n_x, room.n_y))

        LOGGER.debug("mean WER for %s = %f",
                     speech_signal.filename,
                     np.mean(wer_list))

        # pylint: disable=not-callable
        # progress_bar()

    # Normalize WER matrix by the number of samples.
    wer_matrix /= num_samples

    result_dict[job_id] = wer_matrix


def compute_wer_map(dataset: list[SpeechSignal],
                    config: eu.AttrDict = None,
                    **kwargs) -> np.ndarray:

    config = eu.combine_dicts(kwargs, config, DEFAULT_CONFIG)

    VoskAsr.init_vosk(init_gpu=True)

    result_dict: dict[int, np.ndarray] = {}
    threads: list[Thread] = []

    num_samples: int = len(dataset)
    sample_index: int = 0
    thread: Thread
    # Number of samples for each worker
    batch_size: int

    for job_index in range(config.num_jobs):

        if job_index < num_samples % config.num_jobs:
            batch_size = num_samples // config.num_jobs + 1
        else:
            batch_size = num_samples // config.num_jobs
        LOGGER.debug("thread #%i has %i samples", job_index, batch_size)

        start_index: int = sample_index
        end_index: int = sample_index + batch_size

        sample_index += batch_size

        LOGGER.debug("%i --> %i", start_index, end_index - 1)
        sub_ds = dataset[start_index:end_index]
        LOGGER.debug("len(sub_ds) = %i", len(sub_ds))

        thread = Thread(target=_thread_worker,
                        kwargs={'dataset': sub_ds,
                                'result_dict': result_dict,
                                'job_id': job_index,
                                'config': config},
                        name=f'thread-{job_index:02}')

        threads.append(thread)
        thread.start()

    # Wait for every thread to finish
    for thread in threads:
        thread.join()

    # Gather the results.
    total_wer_matrix: np.ndarray = sum(result_dict.values()) / config.num_jobs  # type: ignore

    LOGGER.debug('wer_matrix %s', total_wer_matrix.shape)

    return total_wer_matrix
