#!/usr/bin/env python3

import logging
from multiprocessing import cpu_count
from os.path import isfile, isdir, join
from os import mkdir

import numpy as np
# from alive_progress import alive_bar
import matplotlib.pyplot as plt
import exputils as eu

from rl_audio_nav import asr
from rl_audio_nav.audio_simulator.audio_signal import SpeechSignal
from rl_audio_nav.audio_simulator.wer_map import wer_map_speechbrain

LOGGER: logging.Logger = logging.getLogger(__name__)

CONFIG: eu.AttrDict = eu.AttrDict(
    room=eu.AttrDict(size_x=7, size_y=4,
                     rt_60=0.5,
                     sampling_frequency=16000,
                     grid=eu.AttrDict(delta_x=0.5,
                                      delta_y=0.5)),
    asr='speechbrain',
    # asr='vosk',
    # Vosk config
    num_jobs=cpu_count(),

    # SpeechBrain config

    num_samples=16,
    force=True,
    speech_x=6, speech_y=3,
    noise=False, noise_x=3.5, noise_y=3.5,

    # Dataset
    random=True,
    seed=0
)


def get_folder_path_from_config(config: eu.AttrDict = CONFIG) -> str:
    folder_name: str = ('wer'
                        f'_asr_{config.asr}'
                        f'_T60_{config.room.rt_60}'
                        f'_size_{config.room.size_x}_{config.room.size_y}'
                        f'_delta_{config.room.grid.delta_x}_{config.room.grid.delta_y}'
                        f'_speech_{config.speech_x}_{config.speech_y}'
                        f'_samples_{config.num_samples}')
    if config.noise:
        folder_name += f'_noise_{config.noise_x}_{config.noise_y}'
    # folder_name += '_2_sources'

    folder_path: str = join('output/wer_maps', folder_name)

    return folder_path


def plot_wer_map(wer_matrix: np.ndarray,
                 file_name: str = '') -> None:

    plt.figure()
    # ax = fig.add_subplot()

    imshow = plt.imshow(wer_matrix.T,
                        cmap='bwr',
                        interpolation='none',
                        aspect='equal',
                        extent=[0,
                                CONFIG.room.size_x,
                                0,
                                CONFIG.room.size_y],
                        origin='lower',
                        vmin=0, vmax=1)
    plt.scatter(x=[CONFIG.speech_x],
                y=[CONFIG.speech_y],
                c='g')
    if CONFIG.noise:
        plt.scatter(x=[CONFIG.noise_x],
                    y=[CONFIG.noise_y],
                    c='r')
        plt.scatter(x=[2], y=[1], c='r')
    plt.colorbar(imshow)

    plt.savefig(file_name)

    LOGGER.info("WER map saved to `%s`", file_name)


def main() -> None:
    config: eu.AttrDict = CONFIG

    folder_path: str = get_folder_path_from_config()

    npy_file_path: str = join(folder_path, 'wer_matrix.npy')
    image_file_path: str = join(folder_path, 'wer_map.png')
    if isdir(folder_path) and isfile(npy_file_path) and not config.force:
        LOGGER.debug("Using existing matrix")
        plot_wer_map(wer_matrix=np.load(str(npy_file_path)),
                     file_name=str(image_file_path))
        return

    if not isdir(folder_path):
        mkdir(folder_path)
    config.samples_save_dir = join(folder_path, 'listened_signals')
    if not isdir(config.samples_save_dir):
        mkdir(config.samples_save_dir)

    dataset: list[SpeechSignal] = asr.get_speech_data_set(random=config.random,
                                                          seed=config.seed,
                                                          num_samples=config.num_samples,
                                                          load_audio_array=True)

    LOGGER.info("total num of samples: %i",
                config.num_samples)

    # Gather the results.
    wer_matrix: np.ndarray
    if config.asr == 'vosk':
        wer_matrix = wer_map_vosk.compute_wer_map(dataset=dataset,
                                                  config=config)
    elif config.asr == 'speechbrain':
        wer_matrix = wer_map_speechbrain.compute_wer_map(dataset=dataset,
                                                         config=config)

    # Save the config
    config.to_yaml(path=join(folder_path, 'config.yaml'))

    # Save the matrix
    np.save(file=npy_file_path,
            arr=wer_matrix)

    # Plot the map (save as image).
    plot_wer_map(wer_matrix=wer_matrix,
                 file_name=str(image_file_path))


if __name__ == '__main__':
    main()
