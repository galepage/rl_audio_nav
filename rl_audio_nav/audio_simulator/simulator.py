#!/usr/bin/env python3

"""
DEPRECATED
"""

from typing import Any
import numpy as np

from rl_audio_nav.audio_simulator.room import (Room,
                                               Microphone,
                                               Source)


REFRESH_RATE: int = 20

# TODO maybe autodetect GPU or use a cli flag (click)
GPU: bool = False
if GPU:
    from rl_audio_nav.audio_simulator.room import GpuRirRoom
else:
    from rl_audio_nav.audio_simulator.room import PyRoomAcousticsRoom


class Simulator:

    def __init__(self):
        self.room: Room

        if GPU:
            self.room = GpuRirRoom()
        else:
            self.room = PyRoomAcousticsRoom()

        self.room.add_source(Source(name='speech',
                                    location=np.array([2, 2, 1.2])))
        self.room.add_microphone(Microphone(name='main',
                                            location=np.array([2, 2, 1.2])))

    def run(self):
        audio: int
        agent_position: np.ndarray

        while True:
            pass


def run_simulator() -> None:
    simulator: Simulator = Simulator()

    simulator.run()


if __name__ == '__main__':

    run_simulator()
