"""
Abstract implementation of a shoebox room.
Both backend are supported:
- PyRoomAcoustics (https://github.com/LCAV/pyroomacoustics) implements the Image Source Model and
    a ray tracing algorithm.
- GpuRIR (https://github.com/DavidDiazGuerra/gpuRIR) provides a cuda implementation of the Image
    Source Model.
"""

from abc import ABC
from typing import Iterator
from collections import OrderedDict
from itertools import product
from pathlib import Path
import logging
import exputils as eu

import numpy as np
from scipy.signal import fftconvolve
from scipy.io import wavfile
import pyroomacoustics as pra


DEFAULT_SAMPLING_FREQUENCY: int = 16000


class IllegalMovementException(Exception):
    pass


class AudioObject:
    """
    An AudioObject is either a Microphone or a Source.
    This class gathers common aspects of those.

    Attributes:
        name (str):                         A string to define the AudioObject.
        location (np.ndarray):              The 3d location of the object. shape: (3,)
        orientation (np.ndarray):           A 3d vector encoding the orientation of the
                                                AudioObject. shape: (3,)
        room_dims (np.ndarray):             The dimensions of the room (size_x, size_y, height).
                                                shape: (3,)
        sampling_frequency (np.ndarray):    The sampling frequency associated with this object.
    """

    def __init__(self,
                 name: str,
                 location: np.ndarray,
                 orientation: np.ndarray = np.array([0, 1, 0]),
                 sampling_frequency: int = DEFAULT_SAMPLING_FREQUENCY) -> None:
        """
        Init method.

        Args:
            name (str):                         A string to define the AudioObject.
            location (np.ndarray):              The 3d location of the object. shape: (3,)
            orientation (np.ndarray):           A 3d vector encoding the orientation of the
                                                    AudioObject. shape: (3,)
            sampling_frequency (np.ndarray):    The sampling frequency associated with this object.
        """
        self.name: str = name

        self.location: np.ndarray
        self.set_location(loc=location)

        self.index: int = -1

        self.orientation: np.ndarray
        self.set_orientation(orientation=orientation)

        self.sampling_frequency: int = sampling_frequency

        self.room_dims: np.ndarray

    def set_location(self, loc: np.ndarray) -> None:
        """
        Set the audio object location.

        Args:
            loc (np.ndarray):   A 3d position for the AudioObject.
                                    shape: (3,)
        """
        # Case where a 2D (x, y) vector is provided
        if loc.shape == (2,):
            self.location[:2] = loc
            return

        # Case where a 3D (x, y, z) vector is provided
        assert loc.shape == (3,)
        self.location = loc

    def is_in_room(self) -> bool:
        """
        Check if the object is located in the room.
        """
        return not ((self.location <= 0).any() or (self.location >= self.room_dims).any())

    def set_orientation(self, orientation: np.ndarray) -> None:
        """
        Set the orientation of the microphone

        Args:
            orientation (np.ndarray):   A 3d vector encoding the orientation of the AudioObject.
                                            shape: (3,)
        """
        assert orientation.shape == (3,)

        # Normalize the orientation if needed.
        norm: float = np.linalg.norm(orientation)
        if norm != 1:
            orientation /= norm

        self.orientation = orientation


class Source(AudioObject):
    """
    An audio source.

    Attributes:
        name (str):             A name for this source to easily identify it.
        location (np.ndarray):  Location array (x, y, z) coordinates.
                                    shape: (3,)
        room_dims (np.ndarray): The dimensions of the room (size_x, size_y, height).
                                    shape: (3,)
        signal (np.ndarray):    The audio signal associated to this source.
        delay (float):          A time delay until the source signal starts in the simulation.
    """

    def __init__(self,
                 name: str,
                 location: np.ndarray) -> None:
        """
        Init method.

        Args:
            name (str):             A name for this source to easily identify it.
            location (np.ndarray):  Location array (x, y, z) coordinates.
        """
        super().__init__(name=name,
                         location=location)

        self.signal: np.ndarray
        self.delay: float = 0


class Microphone(AudioObject):
    """
    A microphone.

    Attributes:
        name (str):                     A name for this source to easily identify it.
        location (np.ndarray):          Location array (x, y, z) coordinates.
        room_dims (np.ndarray):         The dimensions of the room (size_x, size_y, height).
                                            shape: (3,)
        pattern (str):                  The microphone pattern
        listened_signal (np.ndarray):   TODO.
    """

    def __init__(self,
                 name: str,
                 location: np.ndarray,
                 pattern: str = 'omni') -> None:
        """
        Init method.

        Args:
            name (str):             A name for this source to easily identify it.
            location (np.ndarray):  Location array (x, y, z) coordinates.
            pattern (str):          The microphone pattern
        """
        super().__init__(name=name,
                         location=location)

        assert pattern in ('omni',
                           'homni',
                           'card',
                           'hypcard',
                           'subcard',
                           'bidir')
        self.pattern: str = pattern
        self.listened_signal: np.ndarray


class Room(ABC):
    """
    Abstract class representing a room.

    Attributes:
        size_x (float):                             The room width (in meters).
        size_y (float):                             The room height (in meters).
        height (float):                             Height of the room (in meters).
        rt_60 (float):                              Desired reverberation time: time for the RIR to
                                                        reach 60dB of attenuation (in seconds).
        max_time (float):                           Length of a RIR (in seconds).
        freq_sampling (int):                        The sampling frequency used in the simulator.
        delta_x (float):                            The distance between two microphones from the
                                                        grid (in the x direction).
        delta_y (float):                            The distance between two microphones from the
                                                        grid (in the y direction).
        n_x (int):                                  The width of the grid (number of microphones
                                                        per row).
        n_y (int):                                  The height of the grid (number of microphones
                                                        per column).
        delta_theta (float):                        Angular sampling resolution (in radians).
        grid_rir (np.ndarray):                      The precomputed RIR at the grid positions.
        sources (OrderedDict[str, Source]):         The audio sources in the room.
        microphones (OrderedDict[str, Microphone]): The microphones in the room.
    """

    default_config: eu.AttrDict = eu.AttrDict(
        size_x=7,
        size_y=4,
        height=2,
        rt_60=0.3,
        max_time=-1,
        sampling_frequency=16000,
        grid=eu.AttrDict(
            delta_x=0.5,
            delta_y=0.5,
            height=1.5,
            add_grid_mics=False)
    )

    def __init__(self,
                 config: eu.AttrDict = None,
                 **kwargs) -> None:
        """
        Init function for a Room object.

        Args:
            size_x (float):             The room width (in meters).
            size_y (float):             The room height (in meters).
            height (float):             Height of the room (in meters).
            rt_60 (float):              Desired reverberation time: time for the RIR to reach 60dB
                                            of attenuation (in seconds).
            max_time (float):           Length of a RIR (in seconds).
            sampling_frequency (int):   The sampling frequency used in the simulator.
        """
        self.config: eu.AttrDict = eu.combine_dicts(kwargs, config, self.default_config)

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)
        self.size_x: float = self.config.size_x
        self.size_y: float = self.config.size_y
        self.height = self.config.height

        # Audio settings
        self.max_time: float = self.config.max_time
        self.sampling_frequency: float = self.config.sampling_frequency

        # Spatial sampling
        self.delta_x: float
        self.delta_y: float
        self.n_x: int
        self.n_y: int

        # precomputed RIR
        self.rir: np.ndarray

        # Reverberation time
        self.rt_60: float = self.config.rt_60

        # If the precomputed RIRs are up to date
        self.rir_up_to_date: bool = False

        # If the audio simulation (i.e. the microphones listened signals) are up to date
        self.simulation_up_to_date: bool = False

        # Sources
        self.sources: OrderedDict[str, Source] = OrderedDict()

        # Microphones
        self.microphones: OrderedDict[str, Microphone] = OrderedDict()

    def init_grid(self,
                  add_grid_mics: bool = False,
                  config: eu.AttrDict = None,
                  **kwargs) -> None:
        """
        Initialize a grid of evenly spaced microphones.

        Args:
            delta_x (float):        Spatial resolution in the x direction. (default: 0.5m)
            delta_y (float):        Spatial resolution in the x direction. (default: 0.5m)
            delta_theta (float):    Angular resolution. (default: pi/4)
            height (float):         The height of the microphone array. (default: 1.5m)
        """
        self.config.grid = eu.combine_dicts(self.config.grid, config, kwargs)

        # Spatial sampling
        self.delta_x = self.config.grid.delta_x
        self.delta_y = self.config.grid.delta_y

        # Check that the height is in the room.
        assert 0 < self.config.grid.height < self.height

        self.n_x = 0
        self.n_y = 0
        prev_mic_x: float = - 1
        prev_mic_y: float = - 1
        for mic_x, mic_y in self.grid_position_iterator():
            if mic_x > prev_mic_x:
                self.n_x += 1
                prev_mic_x = mic_x
            if mic_y > prev_mic_y:
                self.n_y += 1
                prev_mic_y = mic_y

            if add_grid_mics:
                mic: Microphone = Microphone(name=f"grid_{mic_x}_{mic_y}",
                                             location=np.array([mic_x,
                                                                mic_y,
                                                                self.config.grid.height]))

                self.add_microphone(mic=mic)

        if add_grid_mics:
            assert len(self.microphones) == self.n_x * self.n_y

    def get_status_dict(self) -> dict:
        """
        Get a dictionnary representing the current state of the simulation.

        Returns:
            status_dict (dict)
        """
        return {
            'dimensions': {
                'size_x': self.size_x,
                'size_y': self.size_y,
                'height': self.height
            },
            'sources': [  # type: ignore
                {
                    'name': source_name,  # type: ignore
                    'location': source.location  # type: ignore
                }
                for source_name, source in self.sources.values()
            ],
            'microphones': [  # type: ignore
                {
                    'name': mic_name,  # type: ignore
                    'location': mic.location  # type: ignore
                }
                for mic_name, mic in self.microphones.values()
            ]
        }

    def get_room_dims(self) -> np.ndarray:
        """
        Get the room dimensions in a numpy array.

        Returns:
            room_dims (np.ndarray): The dimensions of the room. shape: (3,)
        """
        return np.array([self.size_x, self.size_y, self.height])

    def is_in_room(self, location: np.ndarray) -> bool:
        """
        Checks if a point is inside the room.

        Args:
            location (np.ndarray):  The coordinates of a 2D or 3D point.

        Returns:
            is_in_room (bool):  True if and only if the point is inside the room.
        """
        return bool((location > 0).all() and (location < self.get_room_dims()).all())

    def _get_audio_object_dict(self, audio_object: AudioObject) -> OrderedDict[str, AudioObject]:
        """
        Get the dict of AudioObject (attribute) corresponding to the given AudioObject.
        In practice,
        - if given a Microphone, return `self.microphones`
        - if given a Source, return `self.sources`

        Args:
            audio_object (AudioObject): Either a Microphone or a Source object.

        Returns:
            dest_dict (OrderedDict[str, AudioObject]):  The corresponding dict.
        """
        dest_dict: OrderedDict[str, AudioObject]
        if isinstance(audio_object, Source):
            dest_dict = self.sources  # type: ignore
        elif isinstance(audio_object, Microphone):
            dest_dict = self.microphones  # type: ignore
        else:
            raise Exception(f"Unknown type: {type(AudioObject)}")

        return dest_dict

    def _add(self, audio_object: AudioObject) -> None:
        """
        Private methode to add an AudioObject to its corresponding object.

        Args:
            audio_object (AudioObject):     An audio object.
        """
        dest_dict: OrderedDict[str, AudioObject] = self._get_audio_object_dict(
            audio_object=audio_object)

        assert audio_object.name not in dest_dict, \
            f"{audio_object.name} already exists in the room."

        audio_object.room_dims = self.get_room_dims()

        assert audio_object.is_in_room()

        audio_object.index = len(dest_dict)

        dest_dict[audio_object.name] = audio_object

        self.rir_up_to_date = False
        self.simulation_up_to_date = False

    def add_microphone(self, mic: Microphone) -> None:
        """
        Add a microphone to the room.

        Args:
            mic (Microphone):   A Microphone object.
        """
        self._add(audio_object=mic)

    def add_microphones(self, microphones_list: list[Microphone]) -> None:
        """
        Add several microphones to the list.

        Args:
            microphones_list (list[Microphone]):    A list of Microphone objects.
        """
        for mic in microphones_list:
            self.add_microphone(mic)

    def add_source(self, source: Source) -> None:
        """
        Add an audio source to the room.

        Args:
            source (Source): An audio source.
        """
        self._add(audio_object=source)

    def _move(self,
              audio_object_name: str,
              audio_object_dict: OrderedDict[str, AudioObject],
              new_location: np.ndarray = None,
              delta: np.ndarray = None) -> None:
        """
        Move an audio object to a new location.

        Args:
            audio_object_name (str):                            The name of the object to move.
            new_location (np.ndarray):                          The new position of the object.
            audio_object_dict (OrderedDict[str, AudioObject]):  The dictionnary containing the,
                                                                    AudioObject to move.
            delta (np.ndarray):                                 A displacement vector can be
                                                                    provided instead of an absolute
                                                                    new location:
                                                                    new_location = current_location
                                                                        + delta.
                                                                    Shape=(2-3,)
        """
        assert audio_object_name in audio_object_dict
        assert new_location is None or delta is None

        audio_object: AudioObject = audio_object_dict[audio_object_name]
        if delta is not None:
            if len(delta) == 2:
                delta = np.append(delta, 0)
            assert delta is not None
            new_location = audio_object.location + delta

        assert new_location is not None
        if len(new_location) == 2:
            new_location = np.append(new_location, audio_object.location[2])

        assert new_location is not None
        if not self.is_in_room(location=new_location):
            raise IllegalMovementException

        audio_object.location = new_location

        self.rir_up_to_date = False
        self.simulation_up_to_date = False

    def move_source(self,
                    source_name: str,
                    new_location: np.ndarray = None,
                    delta: np.ndarray = None) -> None:
        """
        Move an audio source to a new location.

        Args:
            source_name (str):          The name of the source to move.
            new_location (np.ndarray):  The new location. Shape=(2-3,)
            delta (np.ndarray):         A displacement vector can be provided instead of an absolute
                                            new location: new_location = current_location + delta.
                                            shape=(2-3,)
        """
        self._move(audio_object_name=source_name,
                   new_location=new_location,
                   delta=delta,
                   audio_object_dict=self.sources)  # type: ignore

    def move_microphone(self,
                        mic_name: str,
                        new_location: np.ndarray = None,
                        delta: np.ndarray = None) -> None:
        """
        Move a microphone to a new location.

        Args:
            mic_name (str):             The name of the microphone to move.
            new_location (np.ndarray):  The new location. Shape=(2-3,)
            delta (np.ndarray):         A displacement vector can be provided instead of an absolute
                                            new location: new_location = current_location + delta.
                                            shape=(2-3,)
        """
        self._move(audio_object_name=mic_name,
                   new_location=new_location,
                   delta=delta,
                   audio_object_dict=self.microphones)  # type: ignore

    def _remove(self,
                audio_object_name: str,
                audio_object_dict: OrderedDict[str, AudioObject]) -> None:
        """
        Private method to remove a source or a microphone.

        Args:
            audio_object_name (str):                            The name of the AudioObject to
                                                                    remove.
            audio_object_dict (OrderedDict[str, AudioObject]):  The dictionnary containing the
                                                                    AudioObject to remove.
        """
        if audio_object_name in audio_object_dict:
            removed_audio_object = audio_object_dict.pop(audio_object_name)

            # Update indices
            for object_index, audio_object in enumerate(audio_object_dict.values()):
                if object_index > removed_audio_object.index:
                    audio_object.index -= 1

            self.rir_up_to_date = False
            self.simulation_up_to_date = False
        else:
            self.logger.error("'%s' does not exist", audio_object_name)

    def get_source_index(self, source_name: str) -> int:
        """
        Get the index of an audio source.

        Args:
            source_name (str):  A source name.
        """
        assert source_name in self.sources
        return self.sources[source_name].index

    def get_source_from_index(self,
                              index: int) -> Source:
        """
        Get the source object corresponding to the given index.

        Args:
            index (int):        The index of the source.

        Returns:
            source (Source):    The corresponding source.
        """
        return list(self.sources.values())[index]

    def get_mic_index(self, mic_name: str) -> int:
        """
        Get the index of a microphone.

        Args:
            mic_name (str):  A microphone name.
        """
        assert mic_name in self.microphones
        return self.microphones[mic_name].index

    def get_mic_from_index(self,
                           index: int = -1) -> Microphone:
        """
        Get the microphone object corresponding to the given index.

        Args:
            index (int):    The index of the microphone.

        Returns:
            mic (Microphone):   The corresponding microphone.
        """
        return list(self.microphones.values())[index]

    def set_source_input_audio_signal(self,
                                      input_audio_signal: np.ndarray,
                                      delay: float = 0,
                                      source_name: str = '',
                                      source_index: int = -1) -> None:
        """
        Set the audio input signal for a given source.

        Args:
            input_audio_signal (np.ndarray):    The mono-aural signal (1D numpy array).
            delay (float):                      The delay before the audio starts.
            source_name (str):                  The name of the source.
            source_index (int):                 Alternatively it is possible to directly provide the
                                                    index of the source.
        """
        assert source_name != '' or source_index >= 0

        assert input_audio_signal.ndim == 1

        source: Source
        if source_name != '':
            assert source_name in self.sources, \
                f"Source '{source_name}' is not in the sources list: {self.sources}"
            source = self.sources[source_name]
        else:
            source = self.get_source_from_index(index=source_index)

        source.signal = input_audio_signal
        source.delay = delay

        self.simulation_up_to_date = False

    def remove_microphone(self, mic_name: str) -> None:
        """
        Remove a microphone from the room.

        Args:
            mic_name (str): A microphone name.
        """
        self._remove(audio_object_name=mic_name,
                     audio_object_dict=self.microphones)  # type: ignore

    def remove_source(self, source_name: str) -> None:
        """
        Remove an audio source from the room.

        Args:
            mic_name (str): A source name.
        """
        self._remove(audio_object_name=source_name,
                     audio_object_dict=self.sources)  # type: ignore

    def get_random_grid_position(self) -> np.ndarray:
        """
        Generate a random 2D position sampled from the grid.

        Returns:
            grid_position (np.ndarray):     The sampled 2D position. shape=(2,)
        """
        x_coord: float = np.random.choice(np.arange(start=0 + self.delta_x / 2,
                                                    stop=self.size_x,
                                                    step=self.delta_x))
        y_coord: float = np.random.choice(np.arange(start=0 + self.delta_y / 2,
                                                    stop=self.size_y,
                                                    step=self.delta_y))

        return np.array([x_coord, y_coord])

    def get_random_position(self, height: float = -1) -> np.ndarray:
        """
        Get the coordinates of a randomly positionned 3d point.

        Args:
            height (float):     Provide a fixed height.

        Returns:
            position (np.ndarray):  The coordinates of the sampled point.
                                        shape: (3,)
        """
        x_coord: float = np.random.random() * self.size_x
        y_coord: float = np.random.random() * self.size_y

        z_coord: float
        if height >= 0:
            z_coord = height
        else:
            z_coord = np.random.random() * self.height

        return np.array([x_coord, y_coord, z_coord])

    def clear_sources(self) -> None:
        """
        Removes all the audio sources from the room.
        """
        while len(self.sources) > 0:
            self.sources.popitem()

        self.rir_up_to_date = False
        self.simulation_up_to_date = False

    def clear_microphones(self) -> None:
        """
        Removes all the microphones from the room.
        """
        while len(self.microphones) > 0:
            self.microphones.popitem()

    def grid_position_iterator(self) -> Iterator[tuple[float, float]]:
        """
        Iterate over the 2d positions from the spatial grid.

        Returns:
            Iterator[Tuple[float, float]]:  An Iterator of 2d coordinates of the grid points.
        """
        for x_coord in np.arange(start=0 + self.delta_x / 2,
                                 stop=self.size_x,
                                 step=self.delta_x):

            for y_coord in np.arange(start=0 + self.delta_y / 2,
                                     stop=self.size_y,
                                     step=self.delta_y):

                yield x_coord, y_coord

    def pre_compute_rir(self) -> None:
        """
        Pre compute the RIR for every source-microphone pair.
        This method is implemented in inheriting classes.
        """
        raise NotImplementedError

    def _online_rir(self,
                    point: np.ndarray,
                    theta: float) -> np.ndarray:
        """
        Compute the RIR for a given position directly.

        Args:
            point (np.ndarray): Coordinates of a 3D point representing the current location of the
                                    agent.
                                    shape=(2,)
            theta (float):      Orientation of the agent.


        Returns:
            rir (np.ndarray):   The Room Impulse Response at this location-orientation.
                                    shape = (self.max_time)
        """
        raise NotImplementedError

    def get_rir(self,
                source_name: str,
                mic_name: str) -> np.ndarray:
        """
        Get the Room Impulse Response function between a source and a microphone.

        Args:
            source_name (str):  The name of the source.
            mic_name (str):     The name of the microphone.

        Returns:
            rir (np.ndarray):   The RIR value (a 1D numpy array).
        """
        if not self.rir_up_to_date:
            self.pre_compute_rir()

        source_index: int = self.get_source_index(source_name=source_name)

        mic_index: int = self.get_mic_index(mic_name=mic_name)

        return self.rir[mic_index, source_index]

    def simulate(self) -> None:
        """
        Compute the audio signal listened by each microphone of the room.
        Code for this function comes from the PyRoomAcoustics library.
        """
        if self.simulation_up_to_date:
            self.logger.info("Useless call to `simulate()`: the simulation is already up to date.")
            return

        if not self.rir_up_to_date:
            self.pre_compute_rir()

        self.logger.debug("Simulating audio")

        n_sources: int = len(self.sources)
        n_mics: int = len(self.microphones)

        max_rir_length = np.array([
            len(self.rir[i][j])
            for i, j in product(range(n_mics), range(n_sources))
        ]).max()

        max_input_signal_length = np.array([
            len(source.signal) + np.floor(source.delay * self.sampling_frequency)
            for source in self.sources.values()
        ]).max()

        simulated_signal_length = int(max_rir_length) + int(max_input_signal_length) - 1
        if simulated_signal_length % 2 == 1:
            simulated_signal_length += 1

        # the array that will receive all the signals
        # premix_signals = np.zeros((n_sources, n_mics, simulated_signal_length))

        # compute the signal at every microphone in the array
        for mic_index, mic in enumerate(self.microphones.values()):

            mic_listened_signal = np.zeros(shape=simulated_signal_length)

            for source_index, source in enumerate(self.sources.values()):
                source_signal: np.ndarray = source.signal
                if source_signal is None:
                    continue

                # Get the precomputed rir
                rir: np.ndarray = self.rir[mic_index][source_index]
                starting_time_index: int = int(np.floor(source.delay * self.sampling_frequency))
                ending_time_index: int = starting_time_index + len(source_signal) + len(rir) - 1

                mic_listened_signal[starting_time_index:ending_time_index] += fftconvolve(
                    in1=rir,
                    in2=source_signal)

            mic_listened_signal = pra.utilities.normalize(signal=mic_listened_signal,
                                                          bits=16)

            mic.listened_signal = np.array(mic_listened_signal, dtype=np.int16)

        self.simulation_up_to_date = True

        # TODO remove
        # premix_signals[source_index,
        #                mic_index,
        #                starting_time_index:ending_time_index] += fftconvolve(
        #     in1=rir,
        #     in2=source_signal)
#
#                 signals: np.ndarray = np.sum(premix_signals, axis=0)

#         if snr is not None:
#             # Normalize all signals so that
#             denom = np.std(premix_signals[:, reference_mic, :], axis=1)
#             premix_signals /= denom[:, None, None]
#
#             # Compute the variance of the microphone noise
#             self.sigma2_awgn = 10 ** (-snr / 10) * n_sources
#
#         else:
#
#         # add white gaussian noise if necessary
#         if self.sigma2_awgn is not None:
#             signals += np.random.normal(0.0, np.sqrt(self.sigma2_awgn), signals.shape)
#
#         # record the signals in the microphones
#         self.mic_array.record(signals, self.fs)

        # return premix_signals

    def get_audio_at_mic(self,
                         mic_name: str = '',
                         mic_index: int = -1,
                         save: bool = False,
                         save_dir: str = 'output/') -> np.ndarray:
        """
        Return the audio signal for a given microphone (using either its name, or directly its
        index).
        This function will start the simulation if needed.

        Args:
            mic_name (str):     A microphone name (optional).
            mic_index (int):    A microphone index (optional).

        Returns:
            target_audio (np.ndarray):  The audio signal at the requested position.
        """
        assert mic_name != '' or mic_index >= 0

        if not (self.rir_up_to_date and self.simulation_up_to_date):
            self.simulate()

        mic: Microphone
        if mic_name != '':
            mic = self.microphones[mic_name]
        else:
            mic = self.get_mic_from_index(index=mic_index)

        if save:
            wavfile.write(filename=str(Path(save_dir) / (mic.name + '.wav')),
                          rate=self.sampling_frequency,
                          data=mic.listened_signal)

        return mic.listened_signal

    def get_audio(self,
                  save: bool = False,
                  save_dir: str = 'output/') -> np.ndarray:
        """
        Return the audio signal for all the microphones.
        This function will start the simulation if needed.

        Returns:
            listened_signals (np.ndarray):  The listened audio signal for each microphone.
                                                shape: (n_mics, T)
        """
        if not (self.rir_up_to_date and self.simulation_up_to_date):
            self.simulate()

        listened_signals: list[np.ndarray] = []
        for mic in self.microphones.values():
            if save:
                wavfile.write(filename=str(Path(save_dir) / (mic.name + '.wav')),
                              rate=self.sampling_frequency,
                              data=mic.listened_signal)
            listened_signals.append(mic.listened_signal)

        assert all(len(listened_signal) == len(listened_signals[0])
                   for listened_signal in listened_signals)

        return np.array(listened_signals)
