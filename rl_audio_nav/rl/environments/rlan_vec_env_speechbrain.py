from typing import Any
from copy import deepcopy

import numpy as np
import exputils as eu
import gym

from ...audio_simulator import SpeechSignal
from .rl_audio_nav_env import RlAudioNavAbstractEnv


class RlAudioNavVecEnv(gym.vector.VectorEnv, RlAudioNavAbstractEnv):

    def __init__(self,
                 num_envs: int,
                 env_config: eu.AttrDict = RlAudioNavAbstractEnv.default_config,
                 copy: bool = True) -> None:

        # TODO, very strangely, the RlAudioNavAbstractEnv.__init__() method is called from the
        # gym.VectorEnv __init__() method (instead of the gym.Env one)
        gym.vector.VectorEnv.__init__(self,
                                      num_envs=num_envs,
                                      observation_space=RlAudioNavAbstractEnv.observation_space,
                                      action_space=RlAudioNavAbstractEnv.action_space)

        RlAudioNavAbstractEnv.__init__(self,
                                       config=env_config,
                                       in_thread=False)

        self.step_counter: int = -1

        self.observations: list[np.ndarray] = []

        self.copy: bool = copy

        self._actions: list[int]

    def reset_wait(self) -> list[np.ndarray]:  # pylint: disable=arguments-differ
        """
        Reset the environment.

        Returns:
            observations (list[np.ndarray]):    Sample from `observation_space` A batch of
                                                    observations from the vectorized environment.
        """
        self._reset()

        # All environments start the same way
        initial_observation: np.ndarray = self._get_obs()[-1]
        observations: list[np.ndarray] = [initial_observation for _ in range(self.num_envs)]

        return observations

    def step_async(self, actions: list[Any]) -> None:
        """
        Args:
            actions (list[Any]):    list of samples from `action_space`.
        """
        assert len(actions) == self.num_envs
        self._actions = actions

    # pylint: disable=arguments-differ
    def step_wait(self) -> tuple[list[np.ndarray],
                                 np.ndarray,
                                 np.ndarray,
                                 list[dict]]:
        """
        Returns:
            observations (list[np.ndarray]):    A batch of observations from the vectorized
                                                    environment.
            rewards (np.ndarray):               `np.ndarray` instance (dtype `np.float_`)
                                                    A vector of rewards from the vectorized
                                                    environment.
            dones (np.ndarray):                 A vector whose entries indicate whether the episode
                                                    has ended.
            infos (list[dict]):                 A list of auxiliary diagnostic information.
        """
        rewards: list[float] = []
        dones: list[bool] = []
        infos: list[dict] = []

        speech_signals: list[SpeechSignal] = []
        listened_signals: list[np.ndarray] = []

        self.observations = []
        self.step_counter += 1

        for action in self._actions:
            info: dict = {
                'step_id': self.step_counter
            }

            done: bool = False

            if self.step_counter >= self.max_number_of_steps:
                done = True
                info['done_reason'] = f"max_number_of_steps ({self.max_number_of_steps}) reached"

            speech_signal, listened_signal, observation = self._move_agent_and_get_obs(
                action=action)

            speech_signals.append(speech_signal)
            listened_signals.append(listened_signal)
            self.observations.append(observation)
            dones.append(done)
            infos.append(info)

        # Run the ASR model
        self.logger.debug("Decoding the signal")
        transcriptions: list[str] = self.asr_decoder.decode_list_of_signal_arrays(
            signal_arrays=listened_signals)
        # TODO remove (it was for testing the pipeline without decoding)
        # transcriptions: list[str] = ['sdfgkjsdlk' for _ in range(len(listened_signals))]

        # Compute WER
        rewards = [self._compute_reward(ground_truth=speech_signal.transcript,
                                        hypothesis=asr_transcript)
                   for speech_signal, asr_transcript in zip(speech_signals, transcriptions)]
        # rewards = [1 for _ in range(self.num_envs)]  # TODO remove

        return (deepcopy(self.observations) if self.copy else self.observations,
                np.array(rewards),
                np.array(dones, dtype=np.bool_),
                infos)

    def render(self, mode: str = 'human') -> None:
        pass

    # pylint: disable=arguments-differ
    def close_extras(self, **kwargs) -> None:
        # TODO check what we usually do in a single RlAudioNavEnv
        pass

    def _check_observation_spaces(self) -> bool:
        return True
