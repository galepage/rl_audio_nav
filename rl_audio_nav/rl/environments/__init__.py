from .rl_audio_nav_env import RlAudioNavEnv
from gym.envs.registration import register
# from .rlan_vec_env_vosk import RlAudioNavVecEnv

register(
    id='rl-audio-nav-v0',
    entry_point='rl_audio_nav.rl.environments:RlAudioNavEnv'
)

register(
    id='rl-audio-nav-vec-vosk-v0',
    entry_point='rl_audio_nav.rl.environments.rlan_vec_env_vosk:RlAudioNavVecEnv'
)

register(
    id='rl-audio-nav-vec-speechbrain-v0',
    entry_point='rl_audio_nav.rl.environments.rlan_vec_env_speechbrain:RlAudioNavVecEnv'
)

register(
    id='rl-audio-nav-wermap-env-v0',
    entry_point='rl_audio_nav.rl.environments.rlan_wer_map:RlanWerMapEnv'
)

register(
    id='rl-audio-nav-wermap-vec-env-v0',
    entry_point='rl_audio_nav.rl.environments.rlan_wer_map:RlanWerMapVecEnv'
)
