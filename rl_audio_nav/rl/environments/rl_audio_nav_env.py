"""
RL Audio navigation environment
"""

from typing import Iterator
import time
import logging

import numpy as np
import gym
from gym import spaces
import scipy
import exputils as eu
from numpy.random import default_rng  # type: ignore

# from .. import ui
from ...asr.speech_data_set import speech_data_set_iterator
from ...asr import AsrDecoder, compute_wer
from ...asr.speechbrain import SpeechBrainAsr
# from ...asr.vosk import VoskAsr
from ...audio_simulator import utils
from ...audio_simulator.audio_signal import SpeechSignal
from ...audio_simulator.room import (Room,
                                     IllegalMovementException,
                                     GpuRirRoom,
                                     PyRoomAcousticsRoom,
                                     Microphone,
                                     Source)

DISCRETE_MOVEMENTS: list[np.ndarray] = [
    np.array([0, 0, 0]),    # Stay (do nothing)
    np.array([0, -1, 0]),   # Up
    np.array([0, 1, 0]),    # Down
    np.array([-1, 0, 0]),   # Left
    np.array([1, 0, 0])     # Right
]

ACTION_NAMES: list['str'] = [
    'stay',
    'up',
    'down',
    'left',
    'right'
]


class StftSpace(gym.Space):

    def contains(self, x) -> bool:
        return isinstance(x, np.ndarray) and x.ndim == 1

    def sample(self) -> np.ndarray:
        """
        Warning: a random spectrogram makes no sense.
        It can maybe be useful for debugging purposes.
        """
        generator: np.random.Generator = default_rng(self.seed()[0])

        # Sample a signal
        # 12.7s is the average duration of a LibriSpeech sample.
        # 3.57s is the standard deviation of the durations.
        length: float = generator.normal(loc=12.7, scale=3.57, size=1)[0]

        signal: np.ndarray = generator.integers(low=np.iinfo(np.int16).min,  # type: ignore
                                                high=np.iinfo(np.int16).max,
                                                dtype=np.int16,
                                                size=length * 16000)

        # TODO return the stft, not the signal
        # Actually, directly sample the STFT...
        return signal


class RlAudioNavAbstractEnv:
    """
    An environment in which the agent has to move to "hear better" (i.e. to maximize the ASR).
    """

    default_config: eu.AttrDict = eu.AttrDict(
        # STFT window length
        stft_window_length=256,

        # Using the room default configuration.
        room=eu.combine_dicts(eu.AttrDict(gpu=True),
                              Room.default_config),

        # agent_initial_position='random',
        agent_initial_position=[2, 1, 1.5],

        # speech_source_position='random',
        speech_source_position=[6, 3, 1.5],

        # ASR backend
        # asr='vosk',
        asr='speechbrain',

        # Use GPU asr
        enable_gpu_asr=True,

        # Maximum number of steps per episode
        max_number_of_steps=32,

        # If positive, crop the listened signal so that its duration is lower than this value:
        max_obs_duration=-1,

        # TODO: useless ? (should belong to the room I think)
        n_channels=1,

        seed=time.time()
    )

    # Define the observation space
    observation_space: gym.Space = StftSpace()

    # Define the action space
    # self.action_space: spaces.Space = spaces.Box(low=-0.5, high=0.5,
    #                                              shape=(2,),
    #                                              dtype=np.float_)
    # 0: STAY (do nothing)
    # 1: UP
    # 2: DOWN
    # 3: LEFT
    # 4: RIGHT
    action_space: gym.Space = spaces.Discrete(len(ACTION_NAMES))

    def __init__(self,
                 config: eu.AttrDict = None,
                 in_thread: bool = False,
                 **kwargs) -> None:
        """
        Args:
            config (AttrDict):  Environment configuration.
            in_thread (bool):   Whether this environment is being started within a thread.
        """
        self.config: eu.AttrDict = eu.combine_dicts(kwargs,
                                                    config,
                                                    RlAudioNavAbstractEnv.default_config)

        self.logger: logging.Logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        # Seed (might be useless)
        self._seed: int = self.config.seed

        # STFT window length
        self.stft_window_length: int = self.config.stft_window_length

        # Room initialization
        self.room: Room
        if self.config.room.gpu:
            self.logger.info("Instanciating a `GpuRirRoom`")
            self.room = GpuRirRoom(config=self.config.room)
        else:
            self.logger.info("Instanciating a `PyRoomAcousticsRoom`")
            self.room = PyRoomAcousticsRoom(config=self.config.room)

        self.config.room = self.room.config

        self.room.init_grid()

        self.asr_decoder: AsrDecoder
        if self.config.asr == 'speechbrain':
            self.asr_decoder = SpeechBrainAsr(use_gpu=self.config.enable_gpu_asr)
        # elif self.config.asr == 'vosk':
        #     self.asr_decoder = VoskAsr(use_gpu=self.config.enable_gpu_asr,
        #                                in_thread=in_thread)

        self.speech_data_set_iterator: Iterator = speech_data_set_iterator()

        # The environment keeps track of the number of steps done.
        self.step_counter: int = -1

        self.max_number_of_steps: int = self.config.max_number_of_steps

        # UI
        # self.user_interface: Optional[ui.Ui] = None

    # def _init_ui(self) -> None:
    #     self.user_interface = ui.Ui()

    def _get_obs(self) -> tuple[SpeechSignal, np.ndarray, np.ndarray]:
        """
        Load sample audio, simulate listened signal and decode it using the ASR module.

        Returns:
            speech_signal (np.ndarray):     Source speech signal.
            listened_signal (np.ndarray):   Simulated listened signal.
            stft (np.ndarray):              Short term Fourier transform of the listened signal.
        """
        # Set new audio signals (iterate over speech and noise)
        speech_signal: SpeechSignal = next(self.speech_data_set_iterator)
        speech_signal.load_audio()
        self.room.set_source_input_audio_signal(input_audio_signal=speech_signal.signal,
                                                source_name='speech_source')

        # Recompute RIR and simulate audio
        self.room.simulate()

        # Simulate and get listened audio. Shape: (N_samples,)
        listened_signal: np.ndarray = self.room.get_audio_at_mic(mic_index=0)

        if self.config.max_obs_duration > 0 \
                and listened_signal.shape[-1] > self.config.max_obs_duration:
            listened_signal = listened_signal[:self.config.max_obs_duration
                                              * self.room.sampling_frequency]

        # Normalize signal and cast it to float 32 before processing STFT
        listened_signal = utils.to_float32(signal=listened_signal)

        self.logger.debug("listened_signal shape: %s", listened_signal.shape)

        # Compute STFT. Shape: (F, L)
        stft: np.ndarray = scipy.signal.stft(x=listened_signal,
                                             fs=self.config.room.sampling_frequency)[-1]
        self.logger.debug("stft shape: %s", stft.shape)

        # Take the modulus of the complex stft coefficients.
        stft = np.absolute(stft)

        return speech_signal, listened_signal, stft

    def _reset(self) -> None:

        self.logger.info("Resetting the environment")

        self.step_counter = 0

        # Reset audio objects locations
        self.room.clear_sources()
        self.room.clear_microphones()

        # Source
        speech_source_position: np.ndarray
        if self.config.speech_source_position not in (None, 'random'):
            speech_source_position = np.array(self.config.speech_source_position)
        elif self.config.speech_source_position == 'random':
            speech_source_position = np.append(self.room.get_random_grid_position(), 1.2)
        self.room.add_source(Source(name='speech_source',
                                    location=speech_source_position))

        # Agent
        agent_initial_position: np.ndarray
        if self.config.agent_initial_position not in (None, 'random'):
            agent_initial_position = np.array(self.config.agent_initial_position)
        elif self.config.agent_initial_position == 'random':
            agent_initial_position = np.append(self.room.get_random_grid_position(), 1.2)
        self.room.add_microphone(Microphone(name='agent',
                                            location=agent_initial_position))

        # Reset dataset
        self.speech_data_set_iterator = speech_data_set_iterator()

    def _compute_reward(self,
                        ground_truth: str,
                        hypothesis: str) -> float:
        self.logger.debug("Computing WER")
        # Ensure the WER is less than 1 so that reward stays between 0 and 1.
        # Actually, it is not a fundamental problem if the reward is negative but a very high WER
        # will not be caused by a 'very bad' positionning. Hence, no need to hardly penalyze the
        # agent in this case.
        reward: float = 1 - min(1, compute_wer(ground_truth=ground_truth,
                                               hypothesis=hypothesis))
        assert 0 <= reward <= 1

        return reward

    def _move_agent_and_get_obs(self,
                                action: int) -> tuple[SpeechSignal, np.ndarray, np.ndarray]:

        delta: np.ndarray = np.array([self.room.delta_x, self.room.delta_y, 0])
        displacement_vector: np.ndarray = DISCRETE_MOVEMENTS[action] * delta

        # Move microphone
        try:
            self.room.move_microphone(mic_name='agent',
                                      delta=displacement_vector)
        except IllegalMovementException:
            # If the movement is not possible (collision with a wall, do nothing)
            self.logger.debug("Invalid movement: ignoring")

        return self._get_obs()

    def render(self, mode: str = 'human') -> None:
        assert mode == 'human'

        grid: list[list[str]] = [['o' for _ in range(self.room.n_x)]
                                 for _ in range(self.room.n_y)]

        source_location: np.ndarray = self.room.get_source_from_index(index=0).location
        source_x: int = int(source_location[0] / self.room.delta_x)
        source_y: int = int(source_location[1] / self.room.delta_y)

        mic_location: np.ndarray = self.room.get_mic_from_index(index=0).location
        agent_x: int = int(mic_location[0] / self.room.delta_x)
        agent_y: int = int(mic_location[1] / self.room.delta_y)

        grid[source_y][source_x] = 'X'
        grid[agent_y][agent_x] = 'A'

        output_str: str = '\n'
        for line in grid:
            for cell in line:
                output_str += cell
            output_str += '\n'

        print(output_str)

    # pylint: disable='signature-differs'
    def seed(self, seed: int) -> list[int]:
        # TODO There might be not
        # return [self.seedself.np_random, seed = seeding.np_random(seed)
        # return [seed]
        if seed:
            self._seed = seed

        return [self._seed]
#
#         self.np_random, seed = seeding.np_random(seed)
#         return [seed]

        #         message: dict = {

#             'type': 'room',
#             'room': self.room.get_status_dict(),
#         }
#
#         if self.user_interface is None:
#             self._init_ui()
#
#         assert self.user_interface is not None
#         self.user_interface.update(message=message)

    def close(self) -> None:
        pass


class RlAudioNavEnv(RlAudioNavAbstractEnv, gym.Env):
    """
    An environment in which the agent has to move to "hear better" (i.e. to maximize the ASR).
    """
    def __init__(self,
                 config: eu.AttrDict = None,
                 in_thread: bool = False) -> None:

        RlAudioNavAbstractEnv.__init__(self,
                                       config=config,
                                       in_thread=in_thread)

    def reset(self) -> np.ndarray:

        self._reset()

        initial_observation: np.ndarray = self._get_obs()[-1]

        return initial_observation

    def step(self, action: int) -> tuple[np.ndarray, float, bool, dict]:
        """
        Perform a step in the environment:
            - Move the microphone according to the given action
            - Select a speech sample
            - Simulate listened signal and compute its STFT (observation)
            - Decode the signal using ASR and compute the reward

        Args:
            action (np.ndarray):    The agent action (a 2D displacement vector).
                                        shape = (2,)

        Returns:
            observation (np.ndarray):   The signal listened by the agent.
            reward (float):             The reward obtained for this step.
                                            reward = 1 - WER
            done (bool):                If the episode is over.
            info (dict):                A dictionnary containing miscellaneous information about the
                                            environment.
        """
        self.step_counter += 1
        info: dict = {
            'step_id': self.step_counter
        }
        done: bool = False
        if self.step_counter >= self.max_number_of_steps:
            done = True
            info['done_reason'] = f"max_number_of_steps ({self.max_number_of_steps}) reached"

        speech_signal, listened_signal, observation = self._move_agent_and_get_obs(action=action)

        # Run the ASR model
        self.logger.debug("Decoding the signal")
        asr_transcript: str = self.asr_decoder.decode(signal=listened_signal)

        # Compute WER
        reward: float = self._compute_reward(ground_truth=speech_signal.transcript,
                                             hypothesis=asr_transcript)

        return observation, reward, done, info
