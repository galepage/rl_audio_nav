"""
RL Audio navigation environment
"""

import sys
import os
from typing import Optional
from pprint import pprint

import yaml
import numpy as np
import gym
import exputils as eu

from .rl_audio_nav_env import RlAudioNavEnv, RlAudioNavAbstractEnv


def _is_map_config_compatible(candidate_config: eu.AttrDict,
                              requested_config: eu.AttrDict) -> bool:
    for req_key, req_value in requested_config.items():
        if req_key not in candidate_config or candidate_config[req_key] != req_value:
            return False

    return True


class RlanWerMapEnv(RlAudioNavEnv):
    """
    An environment in which the agent has to move to "hear better" (i.e. to maximize the ASR).
    """
    default_config: eu.AttrDict = eu.AttrDict(
        wer_map=eu.AttrDict(
            room=eu.AttrDict(size_x=7, size_y=4,
                             rt_60=0.5,
                             sampling_frequency=16000,
                             grid=eu.AttrDict(delta_x=0.5,
                                              delta_y=0.5)),

            speech_x=6, speech_y=3,
            noise=False, noise_x=3.5, noise_y=3.5,
        ),

        # If positive, crop the listened signal so that its duration is lower than this value:
        max_obs_duration=1,
    )

    def __init__(self,
                 config: eu.AttrDict = None,
                 **kwargs) -> None:

        config = eu.combine_dicts(kwargs, config, self.default_config)

        super().__init__(config=config)

        self.wer_map_matrix: np.ndarray = self._get_wer_map()

    def _get_wer_map(self) -> np.ndarray:

        available_maps_folders: list[str] = os.listdir('output/wer_maps')

        wer_map_matrix: Optional[np.ndarray] = None

        for folder in available_maps_folders:
            full_folder_path: str = os.path.join('output/wer_maps', folder)

            try:
                with open(os.path.join(full_folder_path, 'config.yaml'), 'r') as config_file:

                    if _is_map_config_compatible(candidate_config=yaml.load(config_file,
                                                                            yaml.FullLoader),
                                                 requested_config=self.config.wer_map):
                        wer_map_matrix = np.load(os.path.join(full_folder_path, 'wer_matrix.npy'))
                        break
            except FileNotFoundError:
                pass

        if wer_map_matrix is None:
            self.logger.error("There is no pre-computed WER map compatible with the following"
                              " config:")
            pprint(self.config.wer_map)
            sys.exit(1)

        return wer_map_matrix

    def _compute_reward(self) -> float:  # type: ignore
        agent_location: np.ndarray = self.room.get_mic_from_index(index=0).location
        self.logger.debug("Agent location: %s", agent_location)

        # Get closest grid point

        n_x: int = round((agent_location[0] / self.room.delta_x) - 0.5)
        n_y: int = round((agent_location[1] / self.room.delta_y) - 0.5)
        self.logger.debug("n_x: %i", n_x)
        self.logger.debug("n_y: %i", n_y)
        self.logger.debug("coordinates of the microphone: %s",
                          [self.room.delta_x * (0.5 + n_x),
                           self.room.delta_y * (0.5 + n_y)])

        wer: float = min(1, self.wer_map_matrix[n_x][n_y])

        return 1 - wer

    def step(self, action: int) -> tuple[np.ndarray, float, bool, dict]:
        """
        Perform a step in the environment:
            - Move the microphone according to the given action
            - Select a speech sample
            - Simulate listened signal and compute its STFT (observation)
            - Decode the signal using ASR and compute the reward

        Args:
            action (np.ndarray):    The agent action (a 2D displacement vector).
                                        shape = (2,)

        Returns:
            observation (np.ndarray):   The signal listened by the agent.
            reward (float):             The reward obtained for this step.
                                            reward = 1 - WER
            done (bool):                If the episode is over.
            info (dict):                A dictionnary containing miscellaneous information about the
                                            environment.
        """
        self.step_counter += 1
        info: dict = {
            'step_id': self.step_counter
        }
        done: bool = False
        if self.step_counter >= self.max_number_of_steps:
            done = True
            info['done_reason'] = f"max_number_of_steps ({self.max_number_of_steps}) reached"

        _, _, observation = self._move_agent_and_get_obs(action=action)

        # max_index: int =
        # observation = observation[:self.config.]

        # Compute WER
        reward: float = self._compute_reward()

        return observation, reward, done, info


class RlanWerMapVecEnv(gym.vector.VectorEnv):
    """
    TODO: deprecate
    """

    def __init__(self) -> None:
        # TODO
        super().__init__(env_fns=[lambda: RlanWerMapEnv(config=self.config.env_config)
                                  for _ in range(16)],
                         observation_space=RlAudioNavAbstractEnv.observation_space,
                         action_space=RlAudioNavAbstractEnv.observation_space)
