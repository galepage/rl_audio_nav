# References:
# https://github.com/hill-a/stable-baselines/blob/65ed3969e8859092e32e0cf89ac42959a7f283d6/stable_baselines/common/input.py#L6
# https://github.com/hill-a/stable-baselines/blob/65ed3969e8859092e32e0cf89ac42959a7f283d6/stable_baselines/common/distributions.py#L182

import logging
from typing import Callable, Any

import torch
import torch.nn.functional as F
import numpy as np
from gym.spaces import Discrete, Box, MultiDiscrete, Space


LOGGER: logging.Logger = logging.getLogger(__name__)


def preprocess_observetion_space(obs_space: Space,
                                 device: str) -> tuple[int,
                                                       Callable[[Any],
                                                                torch.Tensor]]:
    """
    The `preprocess_obs_fn` receives the observation `x` in the shape of
    `(batch_num,) + obs_space.shape`.
    1) If the `obs_space` is `Discrete`, `preprocess_obs_fn` outputs a
    preprocessed obs in the shape of
    `(batch_num, obs_space.n)`.
    2) If the `obs_space` is `Box`, `preprocess_obs_fn` outputs a
    preprocessed obs in the shape of
    `(batch_num,) + obs_space.shape`.
    In addition, the preprocessed obs will be sent to `device` (either
    `cpu` or `cuda`)
    """
    if isinstance(obs_space, Discrete):
        def preprocess_obs_fn_discrete(obs) -> torch.Tensor:
            return F.one_hot(torch.LongTensor(obs), obs_space.n).float().to(device)

        return obs_space.n, preprocess_obs_fn_discrete

    if isinstance(obs_space, Box):
        def preprocess_obs_fn_box(obs) -> torch.Tensor:
            return torch.Tensor(obs).float().view(torch.Tensor(obs).shape[0], -1).to(device)

        return np.array(obs_space.shape).prod(), preprocess_obs_fn_box

    raise NotImplementedError(
        f"Error: the model does not support input space of type {type(obs_space).__name__}")


def preprocess_action_space(action_space: Space) -> int:
    if isinstance(action_space, Discrete):
        return action_space.n

    if isinstance(action_space, MultiDiscrete):
        return action_space.nvec.sum()

    if isinstance(action_space, Box):
        return np.prod(action_space.shape)

    raise NotImplementedError(
        f"Error: the model does not support output space of type {type(action_space).__name__}")
