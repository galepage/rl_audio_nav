from torch.utils.tensorboard import SummaryWriter


class RlLogger:
    """
    Wrapper for using TensorBoard in RL experiments
    """

    def __init__(self,
                 output_folder: str) -> None:
        self.env_step = env_step
        self.gradient_step = gradient_step
        self.writer = SummaryWriter()

    def log_custom(self,
                   tag: str,
                   epoch: int,
                   value: Any) -> None:
        self.writer.add_scalar(tag=tag,
                               scalar_value=value,
                               global_step=0)

    def log_loss(self,
                 name: str,
                 value: float) -> None:
        global_step: int = epoch * self.gradient_step + step
        log_custom(tag=f'Loss/{name}',
                   value=value,
                   step=global_step)

    def log_policy(self,
                   epoch: int,
                   step: int,
                   policy_loss: float,
                   **kwargs):
        self.writer.add_scalar(tag='Loss/policy_loss',
                               scalar_value=policy_loss,
                               global_step=epoch * self.gradient_step + step)
        if kwargs:
            self.writer.add_scalar(tag='policy_loss/loss',
                                   scalar_value=policy_loss,
                                   global_step=epoch * self.gradient_step + step)

            for key, value in kwargs.items():
                self.writer.add_scalar(tag=f"policy_loss/{key}",
                                       scalar_value=value,
                                       global_step=epoch * self.gradient_step + step)

    def log_q_function(self, epoch, step, q_loss, **kwargs):
        self.writer.add_scalar(tag='Loss/q_loss',
                               scalar_value=q_loss,
                               global_step=epoch * self.gradient_step + step)
        if kwargs:
            self.writer.add_scalar(tag='q_loss/loss',
                                   scalar_value=q_loss,
                                   global_step=epoch * self.gradient_step + step)
            for key, value in kwargs.items():
                self.writer.add_scalar(tag=f"q_loss/{key}",
                                       scalar_value=value,
                                       global_step=epoch * self.gradient_step + step)

    def log_value_function(self, epoch, step, value_loss, **kwargs):
        self.writer.add_scalar(tag='Loss/value_loss',
                               scalar_value=value_loss,
                               global_step=epoch * self.gradient_step + step)
        if kwargs:
            self.writer.add_scalar(tag='value_loss/loss',
                                   scalar_value=value_loss,
                                   global_step=epoch * self.gradient_step + step)
            for key, value in kwargs.items():
                self.writer.add_scalar(tag=f"value _loss/{key}",
                                       scalar_value=value,
                                       global_step=epoch * self.gradient_step + step)

    def log_return(self,
                   epoch,
                   mean_return,
                   **kwargs):
        global_step = epoch * self.env_step
        if kwargs:
            if 'random' in kwargs:
                self.writer.add_scalars(main_tag='Loss/average_return',
                                        tag_scalar_dict={'policy': mean_return,
                                                         'random': kwargs['random']},
                                        global_step=global_step)
            else:
                self.writer.add_scalar(tag='Loss/average_return',
                                       scalar_value=mean_return,
                                       global_step=global_step)

            self.writer.add_scalar(tag='return/loss',
                                   scalar_value=mean_return,
                                   global_step=global_step)
            for key, value in kwargs.items():
                self.writer.add_scalar(tag=f'return/{key}',
                                       scalar_value=value,
                                       global_step=global_step)
        else:
            self.writer.add_scalar("Loss/average_return", mean_return, global_step)

    def close(self) -> None:
        self.writer.flush()
        self.writer.close()
