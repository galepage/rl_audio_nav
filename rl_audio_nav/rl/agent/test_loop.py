"""
A simple loop to test normal and vectorized gym environments by sampling random actions.
"""

import logging

import numpy as np
import gym

# from rl_audio_nav.agent.ppo.ppo import PPOAgent
from rl_audio_nav.rl.environments.rl_audio_nav_env import ACTION_NAMES, RlAudioNavEnv
from rl_audio_nav.rl.environments.rlan_vec_env_speechbrain import RlAudioNavVecEnv
from rl_audio_nav.rl.environments.rlan_wer_map import RlanWerMapEnv
from rl_audio_nav.rl.runner import Runner, VecEnvRunner


N_ENVS: int = 6
N_EPISODES: int = 1 * N_ENVS


class InteractivePolicy:
    def select_action(self,
                      observation: np.ndarray,
                      evaluate: bool = False) -> int:
        print("Available actions:")
        print("\t(0) Stay")
        print("\t(1) Up")
        print("\t(2) Down")
        print("\t(3) Left")
        print("\t(4) Right")
        return int(input("Please, select action: "))


class RandomPolicy:

    def select_action(self,
                      observation: np.ndarray,
                      evaluate: bool = False) -> int:
        return np.random.randint(low=0,
                                 high=len(ACTION_NAMES))

    def select_actions(self,
                       observations: list[np.ndarray],
                       evaluate: bool = False) -> np.ndarray:
        return np.random.randint(low=0,
                                 high=len(ACTION_NAMES),
                                 size=(N_ENVS))


def main() -> None:

    logger: logging.Logger = logging.getLogger(__name__)

    # environment: gym.vector.VectorEnv = RlAudioNavVecEnv(num_envs=N_ENVS)
    environment: gym.Env = RlanWerMapEnv()
    # environment: RlAudioNavEnv = RlAudioNavEnv()

    runner: Runner = Runner(env=environment,
                            render=True)

    # policy = InteractivePolicy()
    policy = RandomPolicy()

    mean: float = runner.test(policy=policy,
                              n_episodes=N_EPISODES)[0]

    logger.info("Mean reward: %f", mean)

    environment.close()


if __name__ == '__main__':
    main()
