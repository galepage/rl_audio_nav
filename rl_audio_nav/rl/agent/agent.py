"""
TODO
"""

from multiprocessing.connection import Connection


class Agent:

    def __init__(self,
                 agent_simulator_pipe: Connection) -> None:
        self.agent_simulator_pipe: Connection = agent_simulator_pipe

    def run(self) -> None:
        while True:
            message: dict = self.agent_simulator_pipe.recv()

            print(message)


def run_agent(**kwargs) -> None:
    agent: Agent = Agent(**kwargs)
    agent.run()
