from abc import ABC, abstractmethod
from typing import Sequence

import numpy as np
from torch.distributions import Distribution, Categorical


def get_action_from_policy(policy: Distribution,
                           evaluate: bool = False) -> np.ndarray:

    if evaluate:
        # Discrete action set
        if isinstance(policy, Categorical):
            action = policy.probs.argmax(-1)

        # Continuous action space
        else:
            action = policy.mean
    else:
        action = policy.sample()
    return action.cpu().numpy()


class Policy(ABC):

    @abstractmethod
    def select_action(self,
                      observation: np.ndarray,
                      evaluate: bool = False) -> int:
        raise NotImplementedError

    def select_actions(self,
                       observations: Sequence[np.ndarray],
                       evaluate: bool = False) -> np.ndarray:

        actions: np.ndarray = np.zeros(len(observations))

        for obs_index, observation in enumerate(observations):
            actions[obs_index] = self.select_action(observation=observation)

        return actions
