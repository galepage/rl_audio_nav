import os
from typing import Any, Union
from datetime import datetime
import time
import logging

import numpy as np
import exputils as eu
import torch
from torch import Tensor
from torch.utils.tensorboard import SummaryWriter
import gym
from alive_progress import alive_bar

from .ppo_agent import PPOAgent
from .loss import ClippedPPOLoss, ClippedValueFunctionLoss
from .gae import GAE
from ...runner import VecEnvRunner

RENDER_AVAILABLE: bool = True
try:
    from pyglet.canvas.xlib import NoSuchDisplayException  # type: ignore
except ImportError:
    RENDER_AVAILABLE = False


class PPO:

    default_config: eu.AttrDict = eu.AttrDict(
        # Number of PPO iterations
        # n_iterations=10_000,
        # n_iterations=1000,
        n_iterations=100,

        # Number of (parallel) workers for sampling data
        n_workers=6,

        # Number of steps to run by each worker for a single update
        n_worker_steps=32,

        # Number of epochs (for a single optimization iteration)
        n_epochs=10,

        # Discount factor (gamma)
        discount_factor=0.99,

        # GAE lambda parameter
        gae_lambda=0.95,

        # Minibatch size
        mini_batch_size=8,

        # Adam learning rate
        optimizer=eu.AttrDict(
            learning_rate=0.0003,
            # (e-5 tip #11 from https://costa.sh/blog-the-32-implementation-details-of-ppo.html)
            epsilon=0.00001
        ),

        # Loss
        loss=eu.AttrDict(
            epsilon_clipping=0.1,
            epsilon_clipping_value_function=0.1,
            value_function_coeff=1,
            entropy_bonus_coeff=0.01
        ),

        # env_name='rl-audio-nav-vec-speechbrain-v0',
        env_name='rl-audio-nav-wermap-env-v0',
        # env_name='CartPole-v1',
        # env_config=eu.AttrDict(),
        env_config=eu.AttrDict(
            room=eu.AttrDict(rt_60=0.5)
        ),

        # PPO agent configuration
        agent=eu.AttrDict(),

        # Seed for `random`, `numpy` and `pytorch`
        seed=int(time.time()),

        torch_deterministic=False,

        # Run a graphical environment to look at performance
        render=True,

        # Default experiment name
        exp_name='ppo'
    )

    def __init__(self,
                 config: eu.AttrDict = None,
                 **kwargs) -> None:

        self.logger: logging.Logger = logging.getLogger(__name__ + '.' + self.__class__.__name__)

        self.config: eu.AttrDict = eu.combine_dicts(kwargs, config, self.default_config)
        self.device: torch.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.logger.debug("PPO device: %s", self.device)
        self.logger.debug("cuda available: %s", torch.cuda.is_available())
        self.logger.debug("torch.cudnn.benchmark: %s", torch.backends.cudnn.benchmark)

        # Seed
        self.seed: int = self.config.seed
        # random.seed(self.seed)
        np.random.seed(self.seed)
        torch.manual_seed(self.seed)
        # If True, causes cuDNN to only use deterministic convolution algorithms.
        torch.backends.cudnn.deterministic = self.config.torch_deterministic

        self.n_iterations: int = self.config.n_iterations
        self.current_iteration: int = 0
        self.n_workers: int = self.config.n_workers
        self.n_worker_steps: int = self.config.n_worker_steps
        self.discount_factor: float = self.config.discount_factor
        self.n_epochs: int = self.config.n_epochs

        # Total number of samples for a single update: TxN
        self.batch_size: int = self.n_workers * self.n_worker_steps
        # Size of a mini batch
        self.mini_batch_size: int = self.config.mini_batch_size
        assert self.batch_size % self.mini_batch_size == 0
        # Number of mini batches
        self.n_mini_batch: int = self.batch_size // self.mini_batch_size

        self.gae: GAE = GAE(n_workers=self.n_workers,
                            n_worker_steps=self.n_worker_steps,
                            gamma=self.discount_factor,
                            lambda_=self.config.gae_lambda)

        # Agent
        self.agent: PPOAgent = PPOAgent(device=self.device,
                                        config=self.config.agent).to(self.device)

        # Loss
        self.value_function_coeff: float = self.config.loss.value_function_coeff
        self.entropy_bonus_coeff: float = self.config.loss.entropy_bonus_coeff
        self.ppo_loss: ClippedPPOLoss = ClippedPPOLoss(
            epsilon_clipping=self.config.loss.epsilon_clipping)

        self.value_loss: ClippedValueFunctionLoss = ClippedValueFunctionLoss(
            epsilon_clipping=self.config.loss.epsilon_clipping_value_function)

        self.optimizer: torch.optim.Optimizer = torch.optim.Adam(
            self.agent.parameters(),
            lr=self.config.optimizer.learning_rate,
            eps=self.config.optimizer.epsilon)

        env: gym.Env = gym.make(id=self.config.env_name,
                                num_envs=self.n_workers,
                                env_config=self.config.env_config)

        # Update the environment config from the one currently used by the environment.
        self.config.env_config = env.config

        self.env: gym.vector.VectorEnv

        if not isinstance(env.unwrapped, gym.vector.VectorEnv):
            self.logger.warning("Environment `%s` was not a vector environment. Creating a vector"
                                " environment from it:", self.config.env_name)
            env.close()
            self.env = gym.vector.make(id=self.config.env_name,
                                       num_envs=self.config.n_workers,
                                       asynchronous=False)
        else:
            self.env = env

        # Number of STFT coefficients (usually: 256//2 + 1 = 129)
        # STFT coefficients: [0, K] with K = N // 2 --> K + 1 coefficients
        self.num_stft_coefs: int = (self.config.env_config.stft_window_length // 2) + 1

        # Logging
        self.save_path_prefix: str = 'output/rl/'
        self.experiment_name: str = (f"{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}"
                                     f"_{self.config.env_name}_{self.config.exp_name}_{self.seed}")
        self.output_folder_path: str = os.path.join('output/rl', self.experiment_name)
        self.writer: SummaryWriter = SummaryWriter(log_dir=self.output_folder_path)
        self.writer.add_text(tag='hyperparameters',
                             text_string="|param|value|\n|-|-|\n%s" % ('\n'.join(
                                 [f"|{key}|{value}|"
                                  for key, value
                                  in self.config.items()])))

        # Initialize

        # initialize tensors for observations
        # Using a list (over a numpy array) because observations are of varying dimensions.
        # `self.obs`: a list of N arrays of shape (F, L_t_n)
        #   - N: number of workers
        #   - F: embedding dimension
        #   - L_t_n: sequence length of the t-th observation (t is the current time step) of the
        #       agent n.
        # self.obs = [None for _ in range(self.n_workers)]
        self.obs: list[np.ndarray] = self.env.reset()

        self.runner: VecEnvRunner = VecEnvRunner(vec_env=self.env)

        self.render: bool = self.config.render
        if self.render and not RENDER_AVAILABLE:
            self.logger.info("Render is not possible.")
            self.render = False

    def _log(self,
             tag: str,
             value: Union[float, Tensor],
             iteration: int = None) -> None:
        if iteration is None:
            iteration = self.current_iteration
        self.writer.add_scalar(tag=tag,
                               scalar_value=value,
                               global_step=iteration)

    def _save_state(self) -> None:
        torch.save(self.agent.state_dict(),
                   os.path.join(self.output_folder_path,
                                f'iter_{self.current_iteration}_agent.pt'))
        torch.save(self.optimizer.state_dict(),
                   os.path.join(self.output_folder_path,
                                f'iter_{self.current_iteration}_optimizer.pt'))

    def _obs_to_torch(self, obs: np.ndarray) -> torch.Tensor:
        """
        Convert a numpy array to a float tensor and send it to the GPU.

        Args:
            obs (np.ndarray):   An observation (or batch of `self.n_workers` observations.

        Returns:
            obs_tensor (Tensor):    The tensor corresponding to the provided observation.
        """
        return torch.tensor(obs,
                            dtype=torch.float32,
                            device=self.device)

    def _test(self) -> None:

        mean: float
        mean, _ = self.runner.test(policy=self.agent,
                                   n_episodes=60)

        self._log(tag='test/mean', value=mean)

    def _add_obs_to_numpy(self,
                          observations: np.ndarray,
                          observation_lengths: np.ndarray,
                          time_step: int) -> np.ndarray:

        # If the observations are cropped, no need to expand the shape of `observations`
        if (not hasattr(self.config.env_config, 'max_obs_duration')) \
                or self.config.env_config.max_obs_duration <= 0:

            # Each `agent_obs` has shape (F, L_t_n)
            # obs_t_max_len = max L_t_n (for n in (1, N))
            obs_t_max_len: int = np.max([agent_obs.shape[-1] for agent_obs in self.obs])

            # If needed (i.e. if L_max_t > L_max_t-1), increase the shape of observations by padding
            # previous observations.
            # observations: (T, N, F, L_max_t-1) -> (T, N, F, L_max_t)
            # L_max_t = max L_t'_n (for t' in (1...t) and n in (1,..,N))
            #         = max {L_max_t-1, obs_t_max_len}
            # L_max_t-1 = observations.shape[-1]
            if obs_t_max_len > observations.shape[-1]:
                observations = np.pad(observations,
                                      pad_width=[(0, 0),
                                                 (0, 0),
                                                 (0, 0),
                                                 (0, obs_t_max_len - observations.shape[-1])])

        # observations should now have shape (T, N, F, L_max_t)

        # observation_lengths contains the length of each observation
        # shape: (T, N), type: int

        # Pad the new observations (time step T) and write them to
        # `observations[T - 1, n, :, :L_T_n]` where L_T_n is the length of the T'th (index T-1)
        # observation of the agent n.
        # time_step in (0,...,T-1)
        # agent_index in (0,...,N-1)
        for agent_index, agent_obs in enumerate(self.obs):
            # agent_obs has shape (F, L_t_n)
            len_t_n: int = agent_obs.shape[-1]
            self.logger.debug('len_t_n: %i', len_t_n)

            # Keep track of each sequence length
            observation_lengths[time_step, agent_index] = len_t_n

            # Copy the observation data in the global matrix
            # observations: Shape=(T, N, F, L_t_n)
            # observations[t, n]: Shape=(F, L_t_n)
            observations[time_step, agent_index, :, :len_t_n] = agent_obs

        return observations

    def _sample(self) -> dict[str, Tensor]:
        """
        Sample data with current policy
        """
        # TODO rethink about the number of time steps.
        # Especially the T+1 th step...

        # shape=(self.n_worker_steps, self.n_workers) = (T, N)
        tn_shape: tuple = (self.n_worker_steps, self.n_workers)

        # Shape (T, N)
        rewards: np.ndarray = np.zeros(shape=tn_shape,
                                       dtype=np.float32)
        actions: np.ndarray = np.zeros(shape=tn_shape,
                                       dtype=np.int32)
        dones: np.ndarray = np.zeros(shape=tn_shape,
                                     dtype=np.bool_)

        # L_0^max = 100
        init_seq_len: int = 100

        # If the observations are cropped, no need to expand the shape of `observations`
        if hasattr(self.config.env_config, 'max_obs_duration') \
                and self.config.env_config.max_obs_duration > 0:
            init_seq_len = self.config.env_config.room.sampling_frequency \
                            * self.config.env_config.max_obs_duration
            self.logger.debug("obs initialized with shape: %s", init_seq_len)
        obs: np.ndarray = np.zeros(shape=(self.n_worker_steps + 1,
                                          self.n_workers,
                                          self.num_stft_coefs,
                                          100),
                                   dtype=np.float32)

        # Shape (T+1, N)
        obs_lengths: np.ndarray = np.zeros(shape=(self.n_worker_steps + 1,
                                                  self.n_workers),
                                           dtype=np.int32)

        # Shape (T, N)
        log_pis: np.ndarray = np.zeros(shape=tn_shape,
                                       dtype=np.float32)
        # Shape (T+1, N)
        values: np.ndarray = np.zeros(shape=(self.n_worker_steps + 1, self.n_workers),
                                      dtype=np.float32)

        with torch.no_grad():
            # Sample `worker_steps` from each worker
            for time_step in range(self.n_worker_steps):
                self.logger.info(f"Time step n°{time_step + 1}/{self.n_worker_steps}")

                # `self.obs` keeps track of the last observation from each worker,
                #  which is the input for the model to sample the next action
                # Update the content of observations (and eventually its shape)
                # `obs` shape: (T, N, F, L_max_t-1) --> (T, N, F, L_max_t)
                obs = self._add_obs_to_numpy(observations=obs,
                                             observation_lengths=obs_lengths,
                                             time_step=time_step)

                # Forwarding the agent with `obs[time_step]` Shape=(N, F, L_max_t)

                # Sample actions from the current policy (pi_theta_old) for each worker.
                # This returns arrays of size `n_workers`
                # pylint: disable=not-callable
                policy_t, values_t = self.agent(obs=self._obs_to_torch(obs=obs[time_step]),
                                                obs_lengths=torch.tensor(obs_lengths[time_step],
                                                                         dtype=torch.int64,
                                                                         device='cpu'))

                values[time_step] = values_t.cpu().numpy()

                # Sampled actions shape: (N,)
                actions_t = policy_t.sample()

                actions[time_step] = actions_t.cpu().numpy()
                log_pis[time_step] = policy_t.log_prob(actions_t).cpu().numpy()

                # Run sampled actions on each worker
                (self.obs,
                 rewards[time_step],
                 dones[time_step],
                 _
                 ) = self.env.step(actions[time_step])

            obs = self._add_obs_to_numpy(observations=obs,
                                         observation_lengths=obs_lengths,
                                         time_step=self.n_worker_steps)

            # Get value (V_T) of after the final step
            # pylint: disable=not-callable
            _, values_t = self.agent(obs=self._obs_to_torch(obs=obs[self.n_worker_steps]),
                                     obs_lengths=torch.tensor(obs_lengths[time_step],
                                                              dtype=torch.int64,
                                                              device='cpu'))
            values[self.n_worker_steps] = values_t.cpu().numpy()

        # calculate advantages
        advantages: np.ndarray = self.gae(dones=dones,
                                          rewards=rewards,
                                          values=values)

        samples: dict[str, Any] = {
            'obs': obs,
            'obs_lengths': obs_lengths,
            'actions': actions,
            'values': values[:-1],
            'log_pis': log_pis,
            'advantages': advantages
        }

        # Converting samples from (N, T, *) to (NxT, *)
        # We should flatten it for training
        samples_flat = {}
        for key, value in samples.items():
            value = value.reshape(value.shape[0] * value.shape[1], *value.shape[2:])
            if key == 'obs':
                samples_flat[key] = self._obs_to_torch(value)
            elif key == 'obs_lengths':
                samples_flat[key] = torch.tensor(value,
                                                 dtype=torch.int64,
                                                 device='cpu')
            else:
                samples_flat[key] = torch.tensor(value,
                                                 device=self.device)

        return samples_flat

    def _train(self,
               samples: dict[str, Tensor]) -> None:
        """
        Train the model based on samples
        """
        self.logger.info("Training phase")
        # It learns faster with a higher number of epochs,
        #  but becomes a little unstable; that is,
        #  the average episode reward does not monotonically increase
        #  over time.
        # May be reducing the clipping range might solve it.

        # `obs` has shape (NxT, F, L_max_T)
        max_seq_len: int = samples['obs'].shape[2]
        for _ in range(self.n_epochs):
            # shuffle for each epoch
            indexes: Tensor = torch.randperm(self.batch_size)

            # For each mini batch
            for start in range(0, self.batch_size, self.mini_batch_size):
                # Get mini batch
                end: int = start + self.mini_batch_size
                mini_batch_indexes: Tensor = indexes[start:end]
                mini_batch: dict = {}

                # Extract tensors from the main `samples` dict and put them in the mini-batch dict
                for key, value in samples.items():
                    mini_batch_value = value[mini_batch_indexes]
                    if key == 'obs':
                        mini_batch_max_seq_len: int = int(torch.max(
                            samples['obs_lengths'][mini_batch_indexes]))
                        if mini_batch_max_seq_len < max_seq_len:
                            mini_batch_value = mini_batch_value[:, :, :mini_batch_max_seq_len]
                            mini_batch_value = mini_batch_value.reshape((self.mini_batch_size,
                                                                         self.num_stft_coefs,
                                                                         mini_batch_max_seq_len))

                    mini_batch[key] = mini_batch_value

                # train
                loss: Tensor = self._calc_loss(samples=mini_batch)

                # Zero out the previously calculated gradients
                self.optimizer.zero_grad()

                # Calculate gradients
                loss.backward()

                # Clip gradients
                torch.nn.utils.clip_grad_norm_(parameters=self.agent.parameters(),
                                               max_norm=0.5)

                # Update parameters based on gradients
                self.optimizer.step()

    @staticmethod
    def _normalize_advantage(adv: Tensor) -> Tensor:
        """
        Normalize advantages.
        """
        return (adv - adv.mean()) / (adv.std() + 1e-8)

    def _calc_loss(self,
                   samples: dict[str, Tensor]) -> Tensor:
        """
        Compute total loss.

        Args:
            samples (dict[str, Tensor]):    The samples gathered using the old policy.

        Returns:
            loss (Tensor):  The total loss.
        """
        # R_t returns sampled from pi_theta_old
        sampled_return: Tensor = samples['values'] + samples['advantages']

        # Normalize the advantages.
        sampled_normalized_advantage: Tensor = self._normalize_advantage(samples['advantages'])

        # Sampled observations are fed into the model to get pi_theta(a_t|s_t) and V^pi_theta(s_t).
        # We are treating observations as state
        # pylint: disable=not-callable
        policy, value = self.agent(obs=samples['obs'],
                                   obs_lengths=samples['obs_lengths'])

        # -log pi_theta (a_t|s_t), a_t are actions sampled from pi_theta_OLD
        log_pi = policy.log_prob(samples['actions'])

        # Calculate policy loss
        policy_loss: float = self.ppo_loss(log_pi=log_pi,
                                           sampled_log_pi=samples['log_pis'],
                                           advantage=sampled_normalized_advantage)

        # Calculate Entropy Bonus
        # L^EB(theta) = E[S[pi_theta](s_t)]
        entropy_bonus: Tensor = policy.entropy()
        entropy_bonus = entropy_bonus.mean()

        # Calculate value function loss
        value_loss: Tensor = self.value_loss(value=value,
                                             sampled_value=samples['values'],
                                             sampled_return=sampled_return)

        # L^(CLIP + VF + EB) = L^CLIP + c_1 L^VF - c_2 L^EB
        loss: Tensor = (policy_loss
                        + self.value_function_coeff * value_loss
                        - self.entropy_bonus_coeff * entropy_bonus)

        # for monitoring
        approx_kl_divergence: Tensor = .5 * ((samples['log_pis'] - log_pi) ** 2).mean()

        self.writer.add_scalar(tag='approx_kl_divergence',
                               scalar_value=approx_kl_divergence,
                               global_step=self.current_iteration)
        self.writer.add_scalar(tag='loss/policy_loss',
                               scalar_value=policy_loss,
                               global_step=self.current_iteration)
        self.writer.add_scalar(tag='loss/value_function_loss',
                               scalar_value=value_loss,
                               global_step=self.current_iteration)
        self.writer.add_scalar(tag='loss/entropy_bonus',
                               scalar_value=entropy_bonus,
                               global_step=self.current_iteration)
        self.writer.add_scalar(tag='loss/total_loss',
                               scalar_value=loss,
                               global_step=self.current_iteration)

        return loss

    def run_training_loop(self) -> None:
        """
        Run training loop
        """
        with alive_bar(self.n_iterations) as progress_bar:
            for iteration in range(self.n_iterations):
                self.current_iteration = iteration

                # Sampling: run the current policy on n_workers parallel workers.
                samples: dict = self._sample()

                # Optimize the network
                self._train(samples)

                # Add a new line to the screen periodically
                if (iteration + 1) % (self.n_iterations // 10) == 0:
                    # print(f"Iteration n°{iteration + 1}/{self.n_iterations}")
                    self._save_state()
                    self._test()
                    if self.render:
                        try:
                            pass
                            # self.runner.render(policy=self.agent,
                            #                    max_step=10_000)
                        except NoSuchDisplayException:
                            self.logger.warning("Seems like no display are available for graphical"
                                                " output --> Disabling rendering.")
                            self.render = False

                # pylint: disable=not-callable
                progress_bar()
