"""
Loss for the PPO algorithm.

credits: https://github.com/labmlai/annotated_deep_learning_paper_implementations
"""

import torch
from torch import nn, Tensor


class ClippedPPOLoss(nn.Module):
    """
    Clipped PPO Loss.
    """

    def __init__(self,
                 epsilon_clipping: float) -> None:

        super().__init__()
        self.epsilon_clipping: float = epsilon_clipping
        self.clip_fraction: Tensor

    def forward(self,
                log_pi: Tensor,
                sampled_log_pi: Tensor,
                advantage: Tensor) -> Tensor:
        """
        Clipped PPO Loss:
            L_CLIP(theta) = E[
                                min(
                                    r_t(theta) x A_t_normalized,
                                    clip(
                                        r_t(theta),
                                        1 - epsilon,
                                        1 + epsilon) x A_t_normalized
                                    )
                            ]

        Args:
            log_pi (Tensor):            log probabilities given by the current policy for the
                                            actions sampled by the old policy.
                                            Shape: (B,)
            sampled_log_pi (Tensor):    log probabilities given by the old policy for the actions
                                            sampled by the old policy.
                                            Shape: (B,)
            advantage (Tensor):         Sampled normalized advantages.
                                            Shape: (B,)

        Returns:
            clipped_ppo_loss (Tensor):  The value of the clipped PPO loss.
                                            Shape: (B,)
        """
        # ratio r_t(theta) = pi_theta (a_t|s_t) / pi_theta_OLD (a_t|s_t)
        # *this is different from rewards* r_t.
        ratio = torch.exp(log_pi - sampled_log_pi)

        # Cliping the policy ratio
        clipped_ratio: Tensor = ratio.clamp(min=1.0 - self.epsilon_clipping,
                                            max=1.0 + self.epsilon_clipping)
        policy_reward: Tensor = torch.min(ratio * advantage,
                                          clipped_ratio * advantage)

        # TODO: useless ?
        self.clip_fraction = (abs((ratio - 1.0)) > self.epsilon_clipping).to(torch.float).mean()

        # -E[...]
        return -policy_reward.mean()


# pylint: disable=too-few-public-methods
class ClippedValueFunctionLoss(nn.Module):
    """
    Clipped Value Function Loss
    Similarly we clip the value function update also.

    Attrs:
        epsilon_clipping (float):   clipping hyperparameter.
    """

    def __init__(self,
                 epsilon_clipping: float) -> None:

        super().__init__()
        self.epsilon_clipping: float = epsilon_clipping

    def forward(self,
                value: Tensor,
                sampled_value: Tensor,
                sampled_return: Tensor) -> Tensor:
        """
        Compute the clipped value function loss.

        Non-clipped:
            L^VF = (V_theta(s_t) - V_t^targ)^2

        Clipped:
            L_CLIP^pi_theta(s_t) = clip(V^pi_theta(s_t) - V_t,
                                        -epsilon, +epsilon)
            L^VF = 1/2 * E[max(
                                (V^pi_theta(s_t)      - R_t)^2,
                                (V_CLIP^pi_theta(s_t) - R_t)^2
                              )]

        Args:
            value (Tensor):             Value estimated by the value network.
                                            --> V_t^targ
                                            Shape: (B,)
            sampled_value (Tensor):     Actual value gathered when running the policy.
                                            --> V_theta(s_t)
                                            Shape: (B,)
            sampled_return (Tensor):    Actual reward

        Returns:
            value_loss (Tensor):    The value of the value function loss.
        """
        # V_t^targ_clipped
        clipped_value: Tensor = sampled_value + (value - sampled_value).clamp(
            min=-self.epsilon_clipping,
            max=self.epsilon_clipping)

        # max[(V_t^targ - Return_t)^2, (V_t^targ_clipped - Return_t)^2]
        vf_loss: Tensor = torch.max((value - sampled_return) ** 2,
                                    (clipped_value - sampled_return) ** 2)

        # L_CLIP^pi_theta(s_t) = (1/2) * E[...]
        return 0.5 * vf_loss.mean()
