import os
from typing import Union

import numpy as np
import torch
from typing import Sequence
from speechbrain.pretrained import EncoderDecoderASR  # type: ignore


from ...audio_simulator import utils
from ...audio_simulator.audio_signal import AudioSignal
from ..asr_decoder import AsrDecoder


def _batch_speech_signals_arrays(
        signal_arrays: Sequence[np.ndarray]) -> tuple[torch.Tensor, torch.Tensor]:

    batch_size: int = len(signal_arrays)
    max_seq_len: int = np.max([len(signal) for signal in signal_arrays])

    padded_tensor: torch.Tensor = torch.zeros(batch_size, max_seq_len)
    lengths: torch.Tensor = torch.zeros(batch_size, dtype=torch.float32)

    # Perform padding
    for signal_index, signal in enumerate(signal_arrays):
        lengths[signal_index] = len(signal) / max_seq_len

        if signal.dtype == np.int16:
            signal = utils.to_float32(signal)
        padded_tensor[signal_index, :len(signal)] = torch.tensor(data=signal,
                                                                 dtype=torch.float32)

    return padded_tensor, lengths


def _batch_speech_signals_objects(
        signal_objects: Sequence[AudioSignal]) -> tuple[torch.Tensor, torch.Tensor]:

    for signal_object in signal_objects:
        signal_object.load_audio_tensor()

    padded_tensor: torch.Tensor = torch.nn.utils.rnn.pad_sequence(
        sequences=[signal_object.signal_tensor for signal_object in signal_objects],
        batch_first=True)
    lengths: torch.Tensor = torch.tensor([len(signal_object.signal_tensor)
                                          for signal_object in signal_objects])

    # Normalize lengths
    lengths = lengths / padded_tensor.shape[1]

    return padded_tensor, lengths


class SpeechBrainAsr(AsrDecoder):

    def __init__(self,
                 sample_rate: int = 16000,
                 use_gpu: bool = True,
                 model_name: str = 'asr-crdnn-rnnlm-librispeech') -> None:

        super().__init__(sample_rate=sample_rate,
                         use_gpu=use_gpu)

        self.logger.debug("HUGGINGFACE_HUB_CACHE=%s", os.getenv("HUGGINGFACE_HUB_CACHE"))
        self.logger.info("model: %s", model_name)

        self.model: EncoderDecoderASR = EncoderDecoderASR.from_hparams(
            source=f"speechbrain/{model_name}",
            savedir=f"rl_audio_nav/asr/speechbrain/pretrained_models/{model_name}",
            run_opts={'device': 'cuda' if self.use_gpu else 'cpu'})

        self.device: torch.device = torch.device('cuda:0' if self.use_gpu else 'cpu')

    def decode(self,
               signal: Union[np.ndarray, torch.Tensor]) -> str:

        signal_tensor: torch.Tensor

        if isinstance(signal, np.ndarray):
            if signal.dtype == np.int16:
                signal = utils.to_float32(signal=signal)

            assert signal.dtype == np.float32

            signal_tensor = torch.tensor(data=signal,
                                         dtype=torch.float32,
                                         device=self.device)

        elif isinstance(signal, torch.Tensor):
            signal_tensor = signal

        # Signal tensor has shape (B, T, C)
        # - B: batch size
        # - T: seq_len (max sequence length)
        # - C: channels (optional)
        if signal_tensor.dim() == 1:
            signal_tensor = signal_tensor.reshape(1, -1)

        lengths: torch.Tensor = torch.tensor([1.0],
                                             dtype=torch.float32,
                                             device=self.device)

        predicted_words: list[str]
        predicted_words, _ = self.model.transcribe_batch(wavs=signal_tensor,
                                                         wav_lens=lengths)

        return ' '.join(predicted_words)

    def decode_tensor(self,
                      signal_tensor: torch.Tensor,
                      lengths: Union[list[int], np.ndarray, torch.Tensor]) -> list[str]:

        if lengths is None:
            assert signal_tensor.dim() == 1

            lengths = torch.tensor(data=[1.0],
                                   dtype=torch.int,
                                   device=self.device)

        elif isinstance(lengths, np.ndarray) or isinstance(lengths, list):

            # Normalize each length
            if isinstance(lengths[0], int):
                max_length: int = np.max(lengths)
                lengths = np.array([length / max_length for length in lengths])

            lengths = torch.tensor(data=lengths,
                                   dtype=torch.float32,
                                   device=self.device)
        else:
            lengths = lengths.to(self.device)

        signal_tensor = signal_tensor.to(self.device)

        transcriptions_list: list[str]
        transcriptions_list, _ = self.model.transcribe_batch(wavs=signal_tensor,
                                                             wav_lens=lengths)

        return transcriptions_list

    def decode_list_of_signal_arrays(self,
                                     signal_arrays: list[np.ndarray]) -> list[str]:

        padded_tensor: torch.Tensor
        lengths: torch.Tensor
        padded_tensor, lengths = _batch_speech_signals_arrays(signal_arrays=signal_arrays)

        return self.decode_tensor(signal_tensor=padded_tensor,
                                  lengths=lengths)

    def decode_list_of_signal_objects(self,
                                      signal_objects: Sequence[AudioSignal]) -> list[str]:

        padded_tensor: torch.Tensor
        lengths: torch.Tensor
        padded_tensor, lengths = _batch_speech_signals_objects(signal_objects=signal_objects)

        return self.decode_tensor(signal_tensor=padded_tensor,
                                  lengths=lengths)
