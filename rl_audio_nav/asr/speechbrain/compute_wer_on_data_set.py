#!/usr/bin/env python3

import logging
from math import ceil

from alive_progress import alive_bar

from rl_audio_nav import asr
from rl_audio_nav.audio_simulator.audio_signal import SpeechSignal

from rl_audio_nav.asr.asr_decoder import AsrDecoder
from rl_audio_nav.asr.speechbrain.speechbrain_asr import SpeechBrainAsr


BATCH_SIZE: int = 1
NUM_SAMPLES: int = 100
RANDOM: bool = True
SEED: int = 0

MODEL_NAME: str
MODEL_NAME = 'asr-transformer-transformerlm-librispeech'
# MODEL_NAME = 'asr-crdnn-transformerlm-librispeech'
# MODEL_NAME = 'asr-crdnn-rnnlm-librispeech'


def main() -> None:
    logger: logging.Logger = logging.getLogger(__name__)

    dataset: list[SpeechSignal] = asr.get_speech_data_set(random=RANDOM,
                                                          seed=0,
                                                          load_audio_tensor=True,
                                                          num_samples=NUM_SAMPLES)

    # Load each sample
    for signal in dataset:
        signal.load_audio_tensor()

    # Sort the samples by lengths to optimize performance
    dataset.sort(key=lambda x: x.n_samples,
                 reverse=True)

    asr_decoder: AsrDecoder = SpeechBrainAsr(use_gpu=True,
                                             model_name=MODEL_NAME)
    total_wer: float = 0

    num_samples: int = len(dataset)
    num_batches: int = ceil(num_samples / BATCH_SIZE)

    logger.info(f"num_samples: {num_samples}")
    logger.info(f"batch_size: {BATCH_SIZE}")
    logger.info(f"num_batches: {num_batches}")

    with alive_bar(num_batches) as progress_bar:
        for batch_index, start_index in enumerate(range(0, num_samples, BATCH_SIZE)):
            end_index = min(start_index + BATCH_SIZE, num_samples)

            batch: list[SpeechSignal] = dataset[start_index:end_index]
            hyp_batch: list[str] = asr_decoder.decode_list_of_signal_objects(signal_objects=batch)

            for signal, hyp in zip(batch, hyp_batch):

                total_wer += asr.compute_wer(ground_truth=signal.transcript,
                                             hypothesis=hyp)

            progress_bar()

    logger.info("mean wer: %.2f%%", 100 * total_wer / num_samples)


if __name__ == '__main__':
    main()
