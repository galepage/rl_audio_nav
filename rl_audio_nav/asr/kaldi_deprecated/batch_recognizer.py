#!/usr/bin/env python

from __future__ import print_function

from kaldi.asr import NnetLatticeFasterBatchRecognizer
from kaldi.decoder import LatticeFasterDecoderOptions
from kaldi.nnet3 import NnetBatchComputerOptions
from kaldi.util.table import SequentialMatrixReader
from kaldi.lat.functions import sentence_level_confidence
from kaldi.cudamatrix import cuda_available
import kaldi.fstext as fst


def main():
    """
    TODO
    """
    if cuda_available():
        from kaldi.cudamatrix import CuDevice
        CuDevice.instantiate().select_gpu_id('yes')
        CuDevice.instantiate().allow_multithreading()

    # Construct recognizer
    decoder_opts = LatticeFasterDecoderOptions()
    decoder_opts.beam = 13
    decoder_opts.max_active = 7000
    compute_opts = NnetBatchComputerOptions()
    compute_opts.acoustic_scale = 1.0
    compute_opts.frame_subsampling_factor = 3
    compute_opts.frames_per_chunk = 150

    final_file = '/home/kali/Desktop/vosk/model/tdnn/final.mdl'
    words_file = '/home/kali/Desktop/vosk/model/graph/words.txt'
    hclg_file = '/home/kali/Desktop/vosk/model/graph/HCLG.fst'
    mfcc_file = '/home/kali/Desktop/vosk/model/tdnn_online/conf/mfcc.conf'
    wav_scp_file = '/home/kali/Desktop/vosk/model/data/test/wav.scp'
    ivector_file = '/home/kali/Desktop/vosk/model/tdnn_online/conf/ivector_extractor.conf'
    spk2utt_file = '/home/kali/Desktop/vosk/model/data/test/spk2utt'
    asr = NnetLatticeFasterBatchRecognizer.from_files(final_file,
                                                      hclg_file,
                                                      words_file,
                                                      decoder_opts=decoder_opts,
                                                      compute_opts=compute_opts,
                                                      num_threads=4)

    # Define feature pipelines as Kaldi rspecifiers
    feats_rspec = f"ark:compute-mfcc-feats --config={mfcc_file} scp:{wav_scp_file} ark:- |"
    ivectors_rspec = (
        f"ark:compute-mfcc-feats --config={mfcc_file} scp:{wav_scp_file} ark:-"
        f" | ivector-extract-online2 --config={ivector_file} ark:{spk2utt_file} ark:- ark:- |"
    )

    # Decode wav files
    with SequentialMatrixReader(feats_rspec) as f, \
         SequentialMatrixReader(ivectors_rspec) as i:
        for (fkey, feats), (ikey, ivectors) in zip(f, i):
            assert fkey == ikey
            asr.accept_input(fkey, (feats, ivectors))
            for out in asr.get_outputs():
                print(out["key"], out["text"], flush=True)
        asr.finished()
        for out in asr.get_outputs():
            print(out["key"], out["text"], flush=True)
            print("****OUT****")
            print(out)
            print("***********")
            confidence = sentence_level_confidence(out["lattice"])
            print("CONFIDENCE:")
            print(confidence)


if __name__ == '__main__':
    main()
