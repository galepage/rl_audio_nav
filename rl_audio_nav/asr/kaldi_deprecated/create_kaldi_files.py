import os
import random
import librosa
import soundfile as sf

def main():

    facebookDB_path = '/home/kali/Desktop/facebookDB/'
    spk2utt_file = '/home/kali/Desktop/vosk/model/data/test/facebookDB/spk2utt'
    wav_scp_file = '/home/kali/Desktop/vosk/model/data/test/facebookDB/wav.scp'
    real_file = '/home/kali/Desktop/vosk/model/data/test/facebookDB/real.txt'
    out_path = '/home/kali/Desktop/facebookDB/results/'

    num_samples = 100
    fs = 16000

    text_file = open(facebookDB_path + 'transcripts.txt', 'r')
    lines = text_file.readlines()
    names = []
    paths = []
    transcripts = []

    for line in lines:
        name, text = line.split('\t')
        splited = name.split('_')
        path = facebookDB_path + 'audio/' + '/'.join(splited[:-1]) + '/' + name + '.opus'

        names.append(name)
        paths.append(path)
        transcripts.append(text)

    random.seed(2411)
    indices = random.sample(range(len(paths)), num_samples)

    paths = [paths[i] for i in indices]
    transcripts = [transcripts[i] for i in indices]
    names = [names[i] for i in indices]

    spk2utt = open(spk2utt_file, 'w')
    wav_scp = open(wav_scp_file, 'w')
    real = open(real_file, 'w')

    for path, text, name in zip(paths, transcripts, names):
        y, sr = librosa.load(path, sr=fs)
        out_file = out_path + name + '.wav'
        sf.write(out_file, y, sr)
        real.write(text[:-1] + '\n')
        wav_scp.write(name + ' ' + out_file + '\n')
        spk2utt.write(name + ' ' + name + '\n')

    spk2utt.close()
    wav_scp.close()
    real.close()


if __name__ == '__main__':
    main()
