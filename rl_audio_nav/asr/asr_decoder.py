import logging
from abc import ABC, abstractmethod
from typing import Sequence

import numpy as np

from ..audio_simulator.audio_signal import AudioSignal


class AsrDecoder(ABC):

    def __init__(self,
                 sample_rate: int = 16000,
                 use_gpu: bool = True) -> None:

        self.sample_rate: int = sample_rate
        self.use_gpu: bool = use_gpu

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)

    @abstractmethod
    def decode(self, signal: np.ndarray) -> str:
        """
        Decode a single signal.

        Args:
            signal (np.ndarray):    The signal to decode. shape: (T,) where T is the number of
                                        samples.

        Returns:
            transcript (str):       The resulting transcript
        """
        raise NotImplementedError

    def decode_list_of_signal_objects(self,
                                      signal_objects: Sequence[AudioSignal]) -> list[str]:
        """
        TODO
        """
        transcriptions: list[str] = []

        signal_object: AudioSignal
        for signal_object in signal_objects:
            signal_object.load_audio()
            transcriptions.append(self.decode(signal_object.signal))

        return transcriptions

    def decode_list_of_signal_arrays(self,
                                     signal_arrays: list[np.ndarray]) -> list[str]:
        """
        TODO
        """
        transcriptions: list[str] = []

        for signal_array in signal_arrays:
            transcriptions.append(self.decode(signal=signal_array))

        return transcriptions
