"""
Vosk ASR system (relying on the kaldi backend).

Vosk: https://alphacephei.com/vosk/
Kaldi: https://www.kaldi-asr.org/
"""

import json
import numpy as np
from ..asr_decoder import AsrDecoder
import vosk


N_FRAMES: int = 4000
DEFAULT_VOSK_LOG_LEVEL: int = 0


class VoskAsr(AsrDecoder):

    def __init__(self,
                 kaldi_recognizer: vosk.KaldiRecognizer = None,
                 model_name: str = 'rl_audio_nav/asr/vosk/model',
                 sample_rate: int = 16000,
                 use_gpu: bool = True,
                 in_thread: bool = False,
                 vosk_log_level: int = DEFAULT_VOSK_LOG_LEVEL) -> None:

        super().__init__(sample_rate=sample_rate,
                         use_gpu=use_gpu)

        self.kaldi_recognizer: vosk.KaldiRecognizer
        if kaldi_recognizer is None:
            self.kaldi_recognizer = vosk.KaldiRecognizer(vosk.Model(model_name),
                                                         self.sample_rate)
        else:
            self.kaldi_recognizer = kaldi_recognizer

        if in_thread and self.use_gpu:
            vosk.GpuThreadInit()
            self.logger.debug("Successfully initiated GPU thread")
        else:
            self.init_vosk(log_level=vosk_log_level,
                           init_gpu=use_gpu)

    @staticmethod
    def init_vosk(log_level: int = DEFAULT_VOSK_LOG_LEVEL,
                  init_gpu: bool = True) -> None:

        vosk.SetLogLevel(log_level)

        # Init the GPU
        if init_gpu:
            vosk.GpuInit()

    def decode(self, signal: np.ndarray) -> str:
        """
        Decode the given audio signal.

        Args:
            signal (np.ndarray):    The audio signal to decode (1D numpy array).

        Returns:
            transcript (str):   The resulting transcript.
        """
        index: int = 0

        for index in range(len(signal) // N_FRAMES):
            start_index: int = index * N_FRAMES
            end_index: int = (index + 1) * N_FRAMES
            data = signal[start_index:end_index].tobytes()

            self.kaldi_recognizer.AcceptWaveform(data)

        results: dict = json.loads(self.kaldi_recognizer.FinalResult())

        transcript: str = results['text']

        return transcript
