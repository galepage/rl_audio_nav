#!/usr/bin/env python3

import time
from datetime import datetime
import logging
import socket
import json
from typing import Callable

import exputils as eu

from rl_audio_nav.audio_simulator.room import Room
from rl_audio_nav.rl.environments import RlAudioNavEnv


DATETIME_FORMAT: str = '%m/%d/%Y-%H:%M:%S'
HOST: str = 'localhost'
PORT: int = 9999


class RemoteServer:

    default_config: eu.AttrDict = eu.AttrDict(
        env=eu.AttrDict(
            room=eu.AttrDict()
        )
    )

    def __init__(self,
                 config: eu.AttrDict = None,
                 **kwargs) -> None:

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)

        # Create a socket (SOCK_STREAM means a TCP socket)
        self.socket: socket.socket = socket.socket(family=socket.AF_INET,
                                                   type=socket.SOCK_STREAM)
        self.socket_connection: socket.socket

        # Simulator
        self.config: eu.AttrDict = eu.combine_dicts(kwargs, config, self.default_config)
        self.env: RlAudioNavEnv = RlAudioNavEnv(config=self.config.env)

        # Get the room initialized by the environment
        self.room: Room = self.env.room

        self._wait_for_client_connection()

        self._send_room_update()

    def _wait_for_client_connection(self) -> None:
        self.logger.info("Binding TCP socket on port %i", PORT)
        not_bound: bool = True
        while not_bound:
            try:
                self.socket.bind((HOST, PORT))
                not_bound = False
            except OSError:
                # Wait before retrying
                self.logger.warning("Port seems to be busy. Retrying...")
                time.sleep(1)

        # Listen for client
        self.logger.info("Listening for connections")
        self.socket.listen()

        addr: tuple[str, int]
        self.logger.info("Waiting for the GUI client to connect to socket...")
        self.socket_connection, addr = self.socket.accept()
        self.logger.info("Got connection. Addr: %s, Port: %i", addr[0], addr[1])

    def _send_dict(self, dic: dict) -> None:
        data: str = json.dumps(dic)
        data_bytes: bytes = data.encode()

        self.socket_connection.sendall(data_bytes)

    def _send_room_update(self) -> None:

        room_status: dict = self.room.get_status_dict()
        self._send_dict(dic=room_status)

    def interactive_simulation(self) -> None:

        while True:
            time.sleep(1)

        #


def _update_loop(update_fn: Callable,
                 socket_connection: socket.socket,
                 task_name: str,
                 refresh_time: int = 0) -> None:

    # Tracking
    init_time: datetime = datetime.now()
    step_counter: int = 1

    logger: logging.Logger = logging.getLogger('rl_hm.data_fetcher - ' + task_name)

    while True:

        current_time: str = datetime.now().strftime(DATETIME_FORMAT)

        logger.info("--> updating")
        logger.info("step n°%i", step_counter)
        logger.info("started: %s", init_time.strftime(DATETIME_FORMAT))

        update_dict: dict = update_fn()

        update_dict.update({
            'timestamp': current_time,
            'source': task_name
        })

        data: str = json.dumps(update_dict)
        logger.debug('len(data) = %i', len(data))
        data_bytes: bytes = data.encode()
        logger.debug('len(data_bytes) = %i', len(data_bytes))

        socket_connection.sendall(data_bytes)

        # from pprint import pprint
        # pprint(update_dict)

        # Eventually wait before updating again
        if refresh_time > 0:
            logger.info("waiting %is before updating again", refresh_time)
            time.sleep(refresh_time)

        step_counter += 1


if __name__ == '__main__':

    server: RemoteServer = RemoteServer()

    server.interactive_simulation()
