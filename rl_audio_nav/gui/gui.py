"""
Graphical User Interface for RlAudioNav
"""

import logging
from multiprocessing.connection import Connection
import socket
import time
from threading import Thread

import numpy as np
# from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg

import rl_audio_nav


class Gui:

    def __init__(self,
                 ui_simulator_pipe: Connection = None) -> None:

        self.logger: logging.Logger = logging.getLogger(self.__class__.__name__)

        ##########
        # SOCKET #
        ##########
        self.port: int = 9999

        self.socket: socket.socket = socket.socket(family=socket.AF_INET,
                                                   type=socket.SOCK_STREAM)

        # TODO remove
        # return

        ##########
        # Qt App #
        ##########
        pg.mkQApp()
        title: str = "RL Audio Navigation"

        self.main_widget: pg.GraphicsLayoutWidget = pg.GraphicsLayoutWidget(show=True)
        self.main_widget.setWindowTitle(title)
        self.main_widget.resize(1400, 800)

        self.main_widget.addLabel(text=title,
                                  size='14pt',
                                  row=0, col=0,
                                  colspan=2)

        # Draw the room widget.
        self.room_widget: pg.PlotItem = self.main_widget.addPlot(row=1, col=0, rowspan=2)
        self.room_widget.enableAutoScale()  # TODO might be useless
        self.room_widget.setMouseEnabled(x=False, y=False)
        self.room_widget.show()

        # Draw the wer widget.
        self.wer_widget: pg.PlotItem = self.main_widget.addPlot(row=1, col=1)
        self.wer_widget.setTitle('Word error rate')
        self.wer_widget.show()

        # Draw the spectrogram widget.
        self.spectrogram_widget: pg.PlotItem = self.main_widget.addPlot(row=2, col=1)
        self.spectrogram_widget.setTitle('Listened signal spectrogram')
        self.spectrogram_widget.show()

        # Run the application
        pg.mkQApp().exec_()

    def connect_to_server(self) -> None:
        self.logger.info("Connecting to server socket on port %i", self.port)
        self.socket.connect(('localhost', self.port))
        self.logger.info("Succesfully connected to the server")

    def run(self) -> None:
        self.logger.info("Running the GUI interface")
        while True:
            time.sleep(1)

    def update_room_widget(self, message: dict) -> None:
        """
        Args:
            message (dict):
        """
        size_x: float = message['room']['dimensions']['size_x']
        size_y: float = message['room']['dimensions']['size_y']
        self.room_widget.plot(x=[0, size_x, size_x, 0],
                              y=[0, 0, size_y, size_y])

    def update_wer_widget(self, message: dict) -> None:
        """
        Args:
            message (dict):
        """

    def update_spectrogram_widget(self, message: dict) -> None:
        """
        Args:
            message (dict):
        """
        audio_signal: np.ndarray = message['signal']

    def update(self, message: dict) -> None:
        """
        Args:
            message (dict):
        """
        message_type: str = message['type']

        if message_type == 'room':
            self.update_room_widget(message=message)

        elif message_type == 'wer':
            self.update_wer_widget(message=message)

        elif message_type == 'stft':
            self.update_spectrogram_widget(message=message)

        else:
            raise Exception(f"The following message type is unknown: {message_type}")

    def update_callback(self) -> None:
        pass
#         while self.socket.poll():
#             # TODO maybe convert
#             message: dict = self.ui_simulator_pipe.recv()
#
#             self.update(message=message)


if __name__ == '__main__':

    gui: Gui = Gui()

    gui.connect_to_server()

    gui.run()
